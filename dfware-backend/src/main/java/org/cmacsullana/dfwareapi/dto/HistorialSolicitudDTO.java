package org.cmacsullana.dfwareapi.dto;

import java.util.Date;

public class HistorialSolicitudDTO {

	private Date fecha;
	private String usuario;
	private String indicEstado;
	private String estado;
	private String indicSituacion;
	private String situacion;
	
	public HistorialSolicitudDTO(Date fecha, String usuario, String indicEstado, String estado, String indicSituacion,
			String situacion) {
		this.fecha = fecha;
		this.usuario = usuario;
		this.indicEstado = indicEstado;
		this.estado = estado;
		this.indicSituacion = indicSituacion;
		this.situacion = situacion;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getIndicEstado() {
		return indicEstado;
	}
	public void setIndicEstado(String indicEstado) {
		this.indicEstado = indicEstado;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getIndicSituacion() {
		return indicSituacion;
	}
	public void setIndicSituacion(String indicSituacion) {
		this.indicSituacion = indicSituacion;
	}
	public String getSituacion() {
		return situacion;
	}
	public void setSituacion(String situacion) {
		this.situacion = situacion;
	}

}
