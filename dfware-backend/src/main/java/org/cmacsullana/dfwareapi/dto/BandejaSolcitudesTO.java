package org.cmacsullana.dfwareapi.dto;

import java.util.Date;

public class BandejaSolcitudesTO {

	private String codTicket;
	private Date fecha;
	private String codServicio;
	private String cliente;
	private String estadoSolicitud;
	private String usuarioRegistra;
	private String usuarioAsignado;
		
	public BandejaSolcitudesTO(String codTicket, Date fecha, String codServicio, String cliente, String estadoSolicitud,
			String usuarioRegistra) {
		this.codTicket = codTicket;
		this.fecha = fecha;
		this.codServicio = codServicio;
		this.cliente = cliente;
		this.estadoSolicitud = estadoSolicitud;
		this.usuarioRegistra = usuarioRegistra;
	}
	
	public BandejaSolcitudesTO(String codTicket, Date fecha, String codServicio, String cliente, String estadoSolicitud,
			String usuarioRegistra,String usuarioAsignado) {
		this.codTicket = codTicket;
		this.fecha = fecha;
		this.codServicio = codServicio;
		this.cliente = cliente;
		this.estadoSolicitud = estadoSolicitud;
		this.usuarioRegistra = usuarioRegistra;
		this.usuarioAsignado=usuarioAsignado;
	}
	
	public String getCodTicket() {
		return codTicket;
	}
	public void setCodTicket(String codTicket) {
		this.codTicket = codTicket;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getCodServicio() {
		return codServicio;
	}
	public void setCodServicio(String codServicio) {
		this.codServicio = codServicio;
	}
	public String getCliente() {
		return cliente;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public String getEstadoSolicitud() {
		return estadoSolicitud;
	}
	public void setEstadoSolicitud(String estadoSolicitud) {
		this.estadoSolicitud = estadoSolicitud;
	}
	public String getUsuarioRegistra() {
		return usuarioRegistra;
	}
	public void setUsuarioRegistra(String usuarioRegistra) {
		this.usuarioRegistra = usuarioRegistra;
	}
	public String getUsuarioAsignado() {
		return usuarioAsignado;
	}
	public void setUsuarioAsignado(String usuarioAsignado) {
		this.usuarioAsignado = usuarioAsignado;
	}
		
}
