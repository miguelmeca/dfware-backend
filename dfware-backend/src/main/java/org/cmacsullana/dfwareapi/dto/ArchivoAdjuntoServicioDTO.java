package org.cmacsullana.dfwareapi.dto;

public class ArchivoAdjuntoServicioDTO {

	private String codigoServicio;
	private String codigoAdjunto;
	private String nombreDocAdjunto;
	private String nombreCarpeta;
	private String indicadorObligatorio;
	
	/*public ArchivoAdjuntoServicioDTO(String codigoServicio, String codigoAdjunto, String nombreCarpeta,
			String indicadorObligatorio) {
		this.codigoServicio = codigoServicio;
		this.codigoAdjunto = codigoAdjunto;
		this.nombreCarpeta = nombreCarpeta;
		this.indicadorObligatorio = indicadorObligatorio;
	}*/

	public ArchivoAdjuntoServicioDTO(String nombreDocAdjunto,String nombreCarpeta,String indicadorObligatorio) {
		this.nombreDocAdjunto = nombreDocAdjunto;
		this.nombreCarpeta = nombreCarpeta;
		this.indicadorObligatorio = indicadorObligatorio;
	}
	
	public ArchivoAdjuntoServicioDTO(String codigoAdjunto,String nombreDocAdjunto,String nombreCarpeta,String indicadorObligatorio) {
		this.codigoAdjunto=codigoAdjunto;
		this.nombreDocAdjunto = nombreDocAdjunto;
		this.nombreCarpeta = nombreCarpeta;
		this.indicadorObligatorio = indicadorObligatorio;
	}
	
	public String getNombreDocAdjunto() {
		return nombreDocAdjunto;
	}

	public void setNombreDocAdjunto(String nombreDocAdjunto) {
		this.nombreDocAdjunto = nombreDocAdjunto;
	}

	public String getCodigoServicio() {
		return codigoServicio;
	}

	public void setCodigoServicio(String codigoServicio) {
		this.codigoServicio = codigoServicio;
	}

	public String getCodigoAdjunto() {
		return codigoAdjunto;
	}

	public void setCodigoAdjunto(String codigoAdjunto) {
		this.codigoAdjunto = codigoAdjunto;
	}

	public String getNombreCarpeta() {
		return nombreCarpeta;
	}

	public void setNombreCarpeta(String nombreCarpeta) {
		this.nombreCarpeta = nombreCarpeta;
	}

	public String getIndicadorObligatorio() {
		return indicadorObligatorio;
	}

	public void setIndicadorObligatorio(String indicadorObligatorio) {
		this.indicadorObligatorio = indicadorObligatorio;
	}
	
	
}
