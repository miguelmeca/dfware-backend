package org.cmacsullana.dfwareapi.model.service.interfaces;

import java.util.List;

import org.cmacsullana.dfwareapi.model.entity.DocumentoPizarra;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface IDocumentoPizarraService {

	public List<DocumentoPizarra> getListDocumentosActivos();
	public DocumentoPizarra guardarDocumentoPizarra(DocumentoPizarra documento);
	public DocumentoPizarra actualizarDocumentoPizarra(DocumentoPizarra documento);
	public DocumentoPizarra obtenerDocumentoByCodigo(String codigoDocumento);
	public String storeFile(MultipartFile file, DocumentoPizarra documento);
	public Resource loadFileAsResource(String fileName)throws Exception;
	public List<DocumentoPizarra> getListDocumentos();	
	public void eliminarArchivo(String nombreArchivo);

}
