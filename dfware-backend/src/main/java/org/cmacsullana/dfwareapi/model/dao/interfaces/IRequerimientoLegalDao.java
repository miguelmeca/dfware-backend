package org.cmacsullana.dfwareapi.model.dao.interfaces;

import java.util.Date;
import java.util.List;

import org.cmacsullana.dfwareapi.dto.BandejaSolcitudesTO;
import org.cmacsullana.dfwareapi.dto.HistorialSolicitudDTO;
import org.cmacsullana.dfwareapi.model.entity.RequerimientoLegal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface IRequerimientoLegalDao extends CrudRepository<RequerimientoLegal, String>{

	List<RequerimientoLegal> findByIndEstadoSolicAndFechaRegTicketAndUsuRegTicket(
				String indEstadoSolic, Date fechaRegTocket, String usuRegTicket);
	
	List<RequerimientoLegal> findByUsuRegTicketAndFechaRegTicket(String usuRegTicket, Date fechaRegTocket);
	
	@Query("SELECT new org.cmacsullana.dfwareapi.dto.BandejaSolcitudesTO(r.codTicketServicio,r.fechaRegTicket, r.codServLegal, "
			+ "r.clienteAsunto, r.indEstadoSolic,r.usuRegTicket) FROM RequerimientoLegal r WHERE r.usuRegTicket=?1 "
			+ "ORDER BY r.codTicketServicio DESC")
	List<BandejaSolcitudesTO> listaBandejaSolcitudes(String codigoUsuario);
	
	
	@Query("SELECT new org.cmacsullana.dfwareapi.dto.BandejaSolcitudesTO(r.codTicketServicio, r.fechaRegTicket, r.codServLegal, r.clienteAsunto, r.indEstadoSolic, "
			+ "r.usuRegTicket, r.usuDirTicket) FROM RequerimientoLegal r INNER JOIN ServicioLegal s ON "
			+ "r.codServLegal= s.codigoServicioLegal WHERE r.indEstadoSolic LIKE ?1 AND r.codServLegal LIKE ?2 AND "
			+ "r.codTicketServicio LIKE ?3 AND r.clienteAsunto LIKE ?4 AND "
			+ "(r.usuRegTicket LIKE ?5 OR r.usuDirTicket LIKE ?5 OR r.usuResTicket LIKE ?5) " 
			+ "ORDER BY r.codTicketServicio DESC")
	List<BandejaSolcitudesTO> findByEstAndServAndTicketAndAsuntoAndUsuario(String estado, String servicio, String ticket,
			String asunto, String usuario);
	
}
