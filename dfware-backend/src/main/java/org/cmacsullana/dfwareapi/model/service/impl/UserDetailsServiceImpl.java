package org.cmacsullana.dfwareapi.model.service.impl;

import org.cmacsullana.dfwareapi.bean.Usuario;
import org.cmacsullana.dfwareapi.model.service.interfaces.IBusIntgracionService;
import org.cmacsullana.dfwareapi.security.UsuarioPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService{

	@Autowired
	IBusIntgracionService iBusIntgracionService;
	
	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		System.out.println("PASO POR AQUI DEBEMOS DE REVISAR ESTA OPCION PARA OBTENER LA CLAVE");
		Usuario usuario = iBusIntgracionService.loginUsuario(username, "sullana");
		return UsuarioPrincipal.build(usuario);
	}

}