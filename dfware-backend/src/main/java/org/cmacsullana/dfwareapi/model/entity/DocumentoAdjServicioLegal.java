package org.cmacsullana.dfwareapi.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="PL_MAE_DOC_ADJUNTO_SERV_LEGAL")
public class DocumentoAdjServicioLegal implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_archivos_adjuntos")
	@SequenceGenerator(sequenceName = "SEQ_COD_DOC_ADJUNTO_SERV_LEGAL",allocationSize = 1, name = "seq_archivos_adjuntos")
	@Column(name = "COD_DOC_ADJ_SERVICIO")
	private Long codigoDocAdjServicio;
	
	@Column(name = "CODIGO_SERVICIO_LEGAL")
	private String codigoServicioLegal;
	
	@Column(name = "CODIGO_DOC_ADJUNTO")
	private String codigoDocumentoAdjunto;
	
	@Column(name = "IND_OBLIGATORIO")
	private String indicadorObligatorio;
	
	@Column(name = "IND_ACTIVO")
	private String indicadorActivo;
	
	@Column(name = "ADICIONADO_POR")
	private String adicionadoPor;
	
	@Column(name = "FECHA_ADICION")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaAdicion;
	
	@Column(name = "MODIFICADO_POR")
	private String modificadoPor;
	
	@Column(name = "FECHA_MODIFICACION")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaModificacion;

	public Long getCodigoDocAdjServicio() {
		return codigoDocAdjServicio;
	}

	public void setCodigoDocAdjServicio(Long codigoDocAdjServicio) {
		this.codigoDocAdjServicio = codigoDocAdjServicio;
	}

	public String getCodigoServicioLegal() {
		return codigoServicioLegal;
	}

	public void setCodigoServicioLegal(String codigoServicioLegal) {
		this.codigoServicioLegal = codigoServicioLegal;
	}

	public String getCodigoDocumentoAdjunto() {
		return codigoDocumentoAdjunto;
	}

	public void setCodigoDocumentoAdjunto(String codigoDocumentoAdjunto) {
		this.codigoDocumentoAdjunto = codigoDocumentoAdjunto;
	}

	public String getIndicadorObligatorio() {
		return indicadorObligatorio;
	}

	public void setIndicadorObligatorio(String indicadorObligatorio) {
		this.indicadorObligatorio = indicadorObligatorio;
	}

	public String getIndicadorActivo() {
		return indicadorActivo;
	}

	public void setIndicadorActivo(String indicadorActivo) {
		this.indicadorActivo = indicadorActivo;
	}

	public String getAdicionadoPor() {
		return adicionadoPor;
	}

	public void setAdicionadoPor(String adicionadoPor) {
		this.adicionadoPor = adicionadoPor;
	}

	public Date getFechaAdicion() {
		return fechaAdicion;
	}

	public void setFechaAdicion(Date fechaAdicion) {
		this.fechaAdicion = fechaAdicion;
	}

	public String getModificadoPor() {
		return modificadoPor;
	}

	public void setModificadoPor(String modificadoPor) {
		this.modificadoPor = modificadoPor;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

}