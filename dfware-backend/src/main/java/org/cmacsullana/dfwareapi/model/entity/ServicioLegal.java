package org.cmacsullana.dfwareapi.model.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "PL_MAE_SERVICIO_LEGAL")
public class ServicioLegal implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "COD_SERVICIO_LEGAL")
	private String codigoServicioLegal;
	
	@Column(name = "NOMBRE_SERVICIO_LEGAL")
	private String nombreServicioLegal;
	
	@Column(name = "INDICA_ESTADO")
	private String indicadorEstado;
	
	@Column(name = "ADICIONADO_POR")
	private String adicionadoPor;
	
	@Column(name = "FECHA_ADICION")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaAdicion;
	
	@Column(name = "MODIFICADO_POR")
	private String modificadoPor;
	
	@Column(name = "FECHA_MODIFICACION")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaModificacion;
	
	@PrePersist
	public void prePersist() {
		indicadorEstado = "A";
		fechaAdicion = new Date();
	}
	
	@PreUpdate
	public void preUpdate() {
		fechaModificacion = new Date();
	}
	
	public ServicioLegal() {
		
	}

	public String getCodigoServicioLegal() {
		return codigoServicioLegal;
	}

	public void setCodigoServicioLegal(String codigoServicioLegal) {
		this.codigoServicioLegal = codigoServicioLegal;
	}

	public String getNombreServicioLegal() {
		return nombreServicioLegal;
	}

	public void setNombreServicioLegal(String nombreServicioLegal) {
		this.nombreServicioLegal = nombreServicioLegal;
	}

	public String getIndicadorEstado() {
		return indicadorEstado;
	}

	public void setIndicadorEstado(String indicadorEstado) {
		this.indicadorEstado = indicadorEstado;
	}

	public String getAdicionadoPor() {
		return adicionadoPor;
	}

	public void setAdicionadoPor(String adicionadoPor) {
		this.adicionadoPor = adicionadoPor;
	}

	public Date getFechaAdicion() {
		return fechaAdicion;
	}

	public void setFechaAdicion(Date fechaAdicion) {
		this.fechaAdicion = fechaAdicion;
	}

	public String getModificadoPor() {
		return modificadoPor;
	}

	public void setModificadoPor(String modificadoPor) {
		this.modificadoPor = modificadoPor;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
		
}
