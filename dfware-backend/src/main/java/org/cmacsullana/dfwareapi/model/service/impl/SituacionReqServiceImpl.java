package org.cmacsullana.dfwareapi.model.service.impl;

import java.util.List;

import org.cmacsullana.dfwareapi.model.dao.interfaces.ISituacionRequisicionDao;
import org.cmacsullana.dfwareapi.model.entity.SituacionSolicitudRequisicion;
import org.cmacsullana.dfwareapi.model.service.interfaces.ISituacionRequisicionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SituacionReqServiceImpl implements ISituacionRequisicionService{

	@Autowired
	ISituacionRequisicionDao iSituacionReqDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<SituacionSolicitudRequisicion> getListSituacionRequisicion() {
		
		return (List<SituacionSolicitudRequisicion>)iSituacionReqDao.findAll();
		
	}

}
