package org.cmacsullana.dfwareapi.model.dao.interfaces;

import java.util.List;

import org.cmacsullana.dfwareapi.dto.ArchivoAdjuntoServicioDTO;
import org.cmacsullana.dfwareapi.model.entity.DocumentoAdjServicioLegal;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface IArchivosAdjuntosDao extends CrudRepository<DocumentoAdjServicioLegal, Long>{

	@Query("SELECT new org.cmacsullana.dfwareapi.dto.ArchivoAdjuntoServicioDTO(d.codDocAdjunto,d.nombreDocAdjunto, d.carpetaDocAdjunto, a.indicadorObligatorio)"
			+ " FROM DocumentoAdjServicioLegal a INNER JOIN DocumentoAdjuntoAndStore d ON a.codigoDocumentoAdjunto=d.codDocAdjunto "
			+ "WHERE a.codigoServicioLegal=?1 AND a.indicadorActivo='S'")
	public List<ArchivoAdjuntoServicioDTO> getListArchivosByCodigoServicio(String codigoServicio);
}
