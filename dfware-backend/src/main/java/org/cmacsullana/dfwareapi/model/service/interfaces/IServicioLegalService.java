package org.cmacsullana.dfwareapi.model.service.interfaces;

import java.util.List;

import org.cmacsullana.dfwareapi.excepciones.BusinessException;
import org.cmacsullana.dfwareapi.model.entity.ServicioLegal;

public interface IServicioLegalService {

	public List<ServicioLegal> getListServiciosActivos();
	public List<ServicioLegal> getListServicios();
	public ServicioLegal actualizarServicio(ServicioLegal servicio);
	public ServicioLegal grabarServicio(ServicioLegal servicio) throws BusinessException;
	public ServicioLegal obtenerServicioByCodigo(String codigoServicio);
	
}
