package org.cmacsullana.dfwareapi.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="PL_SEG_USUARIO")
public class UsuarioDfware implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "CODIGO_USUARIO")
	private String codUsuario;
	
	@Column(name = "NOMBRE_COMPLETO")
	private String nombreCompleto;
	
	@Column(name = "CORREO")
	private String email;
	
	@Column(name = "CODIGO_CARGO")
	private String codigoCargo;
	
	@Column(name = "CARGO")
	private String cargo;
	
	@Column(name = "CODIGO_PERFIL")
	private String codigoPerfil;
	
	@Column(name = "PERFIL")
	private String perfil;
	
	@Column(name = "NUMERO_TELEF_IP")
	private String numeroTelefIp;
	
	@Column(name = "NUMERO_TELEF_MOVIL")
	private String numeroTelefMovil;
	
	@Column(name = "NUMERO_TELEF_FIJO")
	private String numeroTelefFijo;
	
	@Column(name = "INDICADOR_ACTIVO")
	private String indicadorActivo;
	
	@Column(name = "ADICIONADO_POR")
	private String adicionadoPor;
	
	@Column(name = "FECHA_ADICION")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaAdicion;
	
	@Column(name = "MODIFICADO_POR")
	private String modificadoPor;
	
	@Column(name = "FECHA_MODIFICACION")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaModificacion;
	
	@Column(name = "CODIGO_AGENCIA")
	private String codAgencia;
	
	@Column(name = "NOMBRE_AGENCIA")
	private String agencia;

	public UsuarioDfware() {
		
	}
	
	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codigoUsuario) {
		this.codUsuario = codigoUsuario;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCodigoCargo() {
		return codigoCargo;
	}

	public void setCodigoCargo(String codigoCargo) {
		this.codigoCargo = codigoCargo;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getCodigoPerfil() {
		return codigoPerfil;
	}

	public void setCodigoPerfil(String codigoPerfil) {
		this.codigoPerfil = codigoPerfil;
	}

	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

	public String getNumeroTelefIp() {
		return numeroTelefIp;
	}

	public void setNumeroTelefIp(String numeroTelefIp) {
		this.numeroTelefIp = numeroTelefIp;
	}

	public String getNumeroTelefMovil() {
		return numeroTelefMovil;
	}

	public void setNumeroTelefMovil(String numeroTelefMovil) {
		this.numeroTelefMovil = numeroTelefMovil;
	}

	public String getNumeroTelefFijo() {
		return numeroTelefFijo;
	}

	public void setNumeroTelefFijo(String numeroTelefFijo) {
		this.numeroTelefFijo = numeroTelefFijo;
	}

	public String getIndicadorActivo() {
		return indicadorActivo;
	}

	public void setIndicadorActivo(String indicadorActivo) {
		this.indicadorActivo = indicadorActivo;
	}

	public String getAdicionadoPor() {
		return adicionadoPor;
	}

	public void setAdicionadoPor(String adicionadoPor) {
		this.adicionadoPor = adicionadoPor;
	}

	public Date getFechaAdicion() {
		return fechaAdicion;
	}

	public void setFechaAdicion(Date fechaAdicion) {
		this.fechaAdicion = fechaAdicion;
	}

	public String getModificadoPor() {
		return modificadoPor;
	}

	public void setModificadoPor(String modificadoPor) {
		this.modificadoPor = modificadoPor;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getCodAgencia() {
		return codAgencia;
	}

	public void setCodAgencia(String codAgencia) {
		this.codAgencia = codAgencia;
	}

	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

}
