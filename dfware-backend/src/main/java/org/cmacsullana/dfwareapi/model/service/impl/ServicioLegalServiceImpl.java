package org.cmacsullana.dfwareapi.model.service.impl;

import java.util.List;

import org.cmacsullana.dfwareapi.constants.UtilConstants;
import org.cmacsullana.dfwareapi.excepciones.BusinessException;
import org.cmacsullana.dfwareapi.excepciones.RequiredException;
import org.cmacsullana.dfwareapi.model.dao.interfaces.IServicioLegalDao;
import org.cmacsullana.dfwareapi.model.entity.ServicioLegal;
import org.cmacsullana.dfwareapi.model.service.interfaces.IServicioLegalService;
import org.cmacsullana.dfwareapi.utilitarios.AppUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ServicioLegalServiceImpl implements IServicioLegalService{

	private final static Logger logger = LoggerFactory.getLogger(ServicioLegalServiceImpl.class); 
	
	@Autowired
	IServicioLegalDao iServicioLegalDao;
	
	@Override
	public List<ServicioLegal> getListServiciosActivos() {
		return (List<ServicioLegal>) iServicioLegalDao.findByIndicadorEstado("A");
	}
	
	@Override
	public List<ServicioLegal> getListServicios() {
		return (List<ServicioLegal>) iServicioLegalDao.findAllByOrderByCodigoServicioLegalAsc();
	}

	@Override
	@Transactional(rollbackFor = {BusinessException.class, RequiredException.class})
	public ServicioLegal grabarServicio(ServicioLegal servicio) throws BusinessException{
		servicio.setCodigoServicioLegal(AppUtil.generarCodigo("S", iServicioLegalDao.count(), UtilConstants.LONGITUD_CORREL_COD_SERVICIO));
		return iServicioLegalDao.save(servicio);
		
	}
	
	@Override
	@Transactional(rollbackFor = {BusinessException.class, RequiredException.class})
	public ServicioLegal actualizarServicio(ServicioLegal servicio) {
		return iServicioLegalDao.save(servicio);
	}

	@Override
	@Transactional(readOnly = true)
	public ServicioLegal obtenerServicioByCodigo(String codigoServicio) {
	
		return iServicioLegalDao.findById(codigoServicio).orElse(null);
	}
	
	private boolean validarServicioLegalRegistroBasico(ServicioLegal servicio) throws RequiredException, BusinessException{
		boolean respuesta = true;
		if(!AppUtil.validateIsNotNullAndNotEmpty(servicio.getNombreServicioLegal())) {
			respuesta = false;
			throw new RequiredException("Debe de ingresar el nombre!!!");
			
		}
		
		return respuesta;
	}

	
}
