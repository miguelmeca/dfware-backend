package org.cmacsullana.dfwareapi.model.dao.interfaces;

import org.cmacsullana.dfwareapi.model.entity.UsuarioDfware;
import org.springframework.data.repository.CrudRepository;

public interface IUsuarioDfware extends CrudRepository<UsuarioDfware, String>{

	
}
