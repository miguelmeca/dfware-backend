package org.cmacsullana.dfwareapi.model.service.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.persistence.RollbackException;

import org.cmacsullana.dfwareapi.constants.UtilConstants;
import org.cmacsullana.dfwareapi.dto.BandejaSolcitudesTO;
import org.cmacsullana.dfwareapi.dto.HistorialSolicitudDTO;
import org.cmacsullana.dfwareapi.excepciones.BusinessException;
import org.cmacsullana.dfwareapi.excepciones.DocumentoException;
import org.cmacsullana.dfwareapi.excepciones.RequiredException;
import org.cmacsullana.dfwareapi.model.dao.interfaces.IEstadoSolicitudRequisicionDao;
import org.cmacsullana.dfwareapi.model.dao.interfaces.IHistorialSolicituRequerimiento;
import org.cmacsullana.dfwareapi.model.dao.interfaces.IRequerimientoLegalDao;
import org.cmacsullana.dfwareapi.model.entity.HistorialSolicitudRequerimiento;
import org.cmacsullana.dfwareapi.model.entity.RequerimientoLegal;
import org.cmacsullana.dfwareapi.model.entity.UsuarioDfware;
import org.cmacsullana.dfwareapi.model.service.interfaces.IHistorialSolicitudRequerimService;
import org.cmacsullana.dfwareapi.model.service.interfaces.IRequerimientoLegalService;
import org.cmacsullana.dfwareapi.model.service.interfaces.IUsuarioDfwareService;
import org.cmacsullana.dfwareapi.upload.UploadFiles;
import org.cmacsullana.dfwareapi.utilitarios.AppUtil;
import org.cmacsullana.dfwareapi.utilitarios.ConstantesApp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
@Transactional
public class RequerimientoLegalServiceImpl implements IRequerimientoLegalService{

	//private final Path fileStorageLocation;
	
	@Autowired
	IRequerimientoLegalDao iReqLegalDao;
	@Autowired
	IEstadoSolicitudRequisicionDao iEstadoSolictudDao;
	@Autowired
	IHistorialSolicitudRequerimService iHistorial;
	@Autowired
	IUsuarioDfwareService usuarioService;
	@Autowired
	UploadFiles upload;
	
	@Override
	@Transactional(rollbackFor = {BusinessException.class, RequiredException.class})
	public RequerimientoLegal grabarRequerimientoLegal(RequerimientoLegal requerimiento) throws BusinessException{
		RequerimientoLegal requerimientoNuevo= null;
		requerimiento.setCodTicketServicio(AppUtil.generarCodigo("T", iReqLegalDao.count(), 
				UtilConstants.LONGITUD_CORREL_COD_REQUERIMIENTO_LEGAL));
		
		requerimiento.setNumAnioRegistro(AppUtil.fechaFormateada(AppUtil.fechaActual(), "yyyy"));
		requerimiento.setMesAnioregistro(AppUtil.fechaFormateada(AppUtil.fechaActual(), "M"));
		requerimiento.setFechaRegTicket(AppUtil.fechaActual());
		requerimiento.setIndEstadoSolic("ING");
		requerimiento.setIndSituacSolic("PAS");
		requerimientoNuevo = iReqLegalDao.save(requerimiento);
		
		iHistorial.grabarHistorialSolicitud(requerimientoNuevo.getCodTicketServicio(), requerimientoNuevo.getNumAnioRegistro(),
				requerimientoNuevo.getMesAnioregistro(), requerimientoNuevo.getFechaRegTicket(), 
				requerimientoNuevo.getUsuRegTicket(), requerimientoNuevo.getIndEstadoSolic(),
				requerimientoNuevo.getIndSituacSolic(), "REGISTRO DE SOLICITUD A LEGAL");
		
		return requerimientoNuevo;
	}
	
	private String getNombreCarpetaPadre() {
		return AppUtil.fechaFormateada(new Date(), "yyyyMM");
	}
	
	private String getCarpetaHija() {
		return AppUtil.fechaFormateada(new Date(), "dd");
	}
	
	private String getNombreCarpetaAlmacena() {
		String fechaFomateada = AppUtil.fechaFormateada(new Date(), "yyyyMMddHHmmss");
		String keyUnico = UUID.randomUUID().toString();
		return AppUtil.generaCadenaMD5(keyUnico.concat(fechaFomateada));
		
	}
	
	public String directorioAlmacenamiento() {
		StringBuilder directorio = new StringBuilder();
		directorio.append(upload.getDirPadreAlmacena())
			.append(ConstantesApp.RUTA_PRINCIPAL_ARCHIVOS)
			.append(ConstantesApp.SEPARATOR)
			.append(getNombreCarpetaPadre())
			.append(ConstantesApp.SEPARATOR)
			.append(getCarpetaHija())
			.append(ConstantesApp.SEPARATOR)
			.append(getNombreCarpetaAlmacena());
		return directorio.toString();
	}

	
	public void almacenArchivos(List<MultipartFile>listaFiles, String directorio) { 
		//Crear el directorio de almacenamiento
		StringBuilder direcArchivo = new StringBuilder();
		direcArchivo.append(directorio).append(ConstantesApp.SEPARATOR)
		.append("SOLIC_CRED_APROBADA");
		
		String nombreCarpeta = direcArchivo.toString();
		
		Path path = Paths.get(nombreCarpeta.toString()).toAbsolutePath().normalize();
		
		try {
			Files.createDirectories(path);
		} catch (Exception ex) {
			throw new DocumentoException(
					"No se pudo crear el directorio en donde se cargaran los archivos subidos.",
					ex
					);
		}
		
		for(MultipartFile archivo : listaFiles) {
				
			String originalFileName = StringUtils.cleanPath(archivo.getOriginalFilename());
				if (originalFileName.contains("..")) {
					throw new DocumentoException(
							"Atención! El nombre del archivo carácteres invalidos en la ruta " + originalFileName);
				}
				try {
					Files.copy(archivo.getInputStream(), path.resolve(originalFileName), StandardCopyOption.REPLACE_EXISTING);
					
				} catch (IOException e) {
					throw new DocumentoException("No se pudo almacenar el archivo " + originalFileName + ". Intentelo nuevamente!", e);
				}
		}
	}

	public Map<String, Object> estadisticaByIndEstadoSolicAndFechaRegTicketAndUsuRegTicket(String usuRegTicket) {
		
		Map<String, Object> estadistica = new HashMap<>();
		estadistica.put("ING", iReqLegalDao.findByIndEstadoSolicAndFechaRegTicketAndUsuRegTicket("ING",new Date(), usuRegTicket).size());
		
		return estadistica;
	}

	@Override
	public List<RequerimientoLegal> findByUsuRegTicketAndFechaRegTicket(String usuRegTicket, Date fechaRegTocket) {
		return iReqLegalDao.findByUsuRegTicketAndFechaRegTicket(usuRegTicket, fechaRegTocket);
	}

	@Override
	public List<BandejaSolcitudesTO> listaBandejaSolcitudes(String codigoUsuario) {
		List<BandejaSolcitudesTO> lista =null;
		UsuarioDfware usuario = usuarioService.buscarUsuarioByCodigo(codigoUsuario);
		if(usuario != null) {
			String codPerfil = usuario.getCodigoPerfil().toUpperCase();
			if(codPerfil.equals("GER")) {
				lista = iReqLegalDao.findByEstAndServAndTicketAndAsuntoAndUsuario("%", "%", "%", "%", "%");
			}else if(codPerfil.equals("ADN")) {
				lista = iReqLegalDao.findByEstAndServAndTicketAndAsuntoAndUsuario("%", "%", "%", "%", codigoUsuario);
			}
		}
		
		return lista;
	}
	
	@Transactional(rollbackFor = {RequiredException.class})
	public void consultaDetalleSolicitud(String codTicket, String codUsuario) {
		RequerimientoLegal requerimientoActual = iReqLegalDao.findById(codTicket).orElse(null);
		System.out.println("requerimiento legal buscado:"+requerimientoActual);
		if(requerimientoActual != null) {
			grabarHistorialRequerimiento(codTicket,codUsuario,requerimientoActual.getIndEstadoSolic(), "REV", "","");
		}
	}
	
	@Transactional(rollbackFor = {RequiredException.class})
	public RequerimientoLegal detalleRequerimientoByCodTicket(String codTicket) {
		
		return iReqLegalDao.findById(codTicket).orElse(null);
		
	}
	
	private void grabarHistorialRequerimiento(String numeroTicket, String codUsuario, String estadoActualTicket, 
			String estadoNuevoTicket, String situacionTicket, String motivo) {
		
		/**
		 * LEYENDA DE CODIGOS DEL ESTADO DE LA SOLICITUD
		 * 
		 * CONC: CONCLUIDA
		 * REV: REVISADA
		 * */
		if(!estadoActualTicket.equals("CONC")) {
			if(estadoNuevoTicket.equals("REV")) {
				iHistorial.grabarHistorialSolicitud(numeroTicket, AppUtil.fechaFormateada(new Date(), "YYYY"),
						AppUtil.fechaFormateada(new Date(), "M"), new Date(), codUsuario, estadoNuevoTicket,
						situacionTicket, motivo);
			}
		}
	}
	
	

	/*@Override
	public List<RequerimientoLegal> buscarByIndEstadoSolicAndFechaRegTicketAndUsuRegTicket(String indEstadoSolic,
			Date fechaRegTocket, String usuRegTicket) {
		return iReqLegalDao.findByIndEstadoSolicAndFechaRegTicketAndUsuRegTicket(indEstadoSolic, fechaRegTocket, usuRegTicket);
	}*/

}
