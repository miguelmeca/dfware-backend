package org.cmacsullana.dfwareapi.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "PL_HIS_SOLIC_REQ_LEGAL")
public class HistorialSolicitudRequerimiento implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_historial")
	@SequenceGenerator(sequenceName = "SEQ_COD_HISTORIAL_SOLICITUD",allocationSize = 1, name = "seq_historial")
	@Column(name = "COD_HISTORIAL", nullable = false, unique = true)
	private Long codHistorial;
		
	@Column(name = "TICKET_SERVICIO")
	private String codTicketServicio;
	
	@Column(name = "NUM_ANIO_REG")
	private String numAnioRegistro;
	
	@Column(name = "NUM_MES_REG")
	private String mesAnioregistro;
	
	@Column(name = "FECHA_OPERACION")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaMovimiento;
	
	@Column(name = "COD_USU_REALIZA_MOVIM")
	private String usuMovimiento;
	
	@Column(name = "IND_ESTADO_SOLIC")
	private String indEstadoSolic;
	
	@Column(name = "IND_SITUACION_SOLIC")
	private String indSituacSolic;
	
	@Column(name = "DESCRIPC_MOTIVO")
	private String motivo;
	
	
	public Long getCodHistorial() {
		return codHistorial;
	}

	public void setCodHistorial(Long codHistorial) {
		this.codHistorial = codHistorial;
	}

	public String getCodTicketServicio() {
		return codTicketServicio;
	}

	public void setCodTicketServicio(String codTicketServicio) {
		this.codTicketServicio = codTicketServicio;
	}

	public String getNumAnioRegistro() {
		return numAnioRegistro;
	}

	public void setNumAnioRegistro(String numAnioRegistro) {
		this.numAnioRegistro = numAnioRegistro;
	}

	public String getMesAnioregistro() {
		return mesAnioregistro;
	}

	public void setMesAnioregistro(String mesAnioregistro) {
		this.mesAnioregistro = mesAnioregistro;
	}

	public Date getFechaMovimiento() {
		return fechaMovimiento;
	}

	public void setFechaMovimiento(Date fechaMovimiento) {
		this.fechaMovimiento = fechaMovimiento;
	}

	public String getUsuMovimiento() {
		return usuMovimiento;
	}

	public void setUsuMovimiento(String usuMovimiento) {
		this.usuMovimiento = usuMovimiento;
	}

	public String getIndEstadoSolic() {
		return indEstadoSolic;
	}

	public void setIndEstadoSolic(String indEstadoSolic) {
		this.indEstadoSolic = indEstadoSolic;
	}

	public String getIndSituacSolic() {
		return indSituacSolic;
	}

	public void setIndSituacSolic(String indSituacSolic) {
		this.indSituacSolic = indSituacSolic;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	
}
