package org.cmacsullana.dfwareapi.model.dao.interfaces;

import java.util.List;

import org.cmacsullana.dfwareapi.model.entity.ServicioLegal;
import org.springframework.data.repository.CrudRepository;

public interface IServicioLegalDao extends CrudRepository<ServicioLegal, String>{

	List<ServicioLegal> findByIndicadorEstado(String indicadorEstado);
			
	List<ServicioLegal> findAllByOrderByCodigoServicioLegalAsc();
		
}
