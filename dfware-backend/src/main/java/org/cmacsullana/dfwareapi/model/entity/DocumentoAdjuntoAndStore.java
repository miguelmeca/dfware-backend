package org.cmacsullana.dfwareapi.model.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="PL_MAE_DOCUM_ADJUNTO")
public class DocumentoAdjuntoAndStore implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="COD_DOCUM_ADJUNTO")
	private String codDocAdjunto;
	
	@Column(name="NOMBRE_DOCUM_ADJUNTO")
	private String nombreDocAdjunto;
	
	@Column(name="CARPETA_DOC_ADJUNTO")
	private String carpetaDocAdjunto;

	public String getCodDocAdjunto() {
		return codDocAdjunto;
	}

	public void setCodDocAdjunto(String codDocAdjunto) {
		this.codDocAdjunto = codDocAdjunto;
	}

	public String getNombreDocAdjunto() {
		return nombreDocAdjunto;
	}

	public void setNombreDocAdjunto(String nombreDocAdjunto) {
		this.nombreDocAdjunto = nombreDocAdjunto;
	}

	public String getCarpetaDocAdjunto() {
		return carpetaDocAdjunto;
	}

	public void setCarpetaDocAdjunto(String carpetaDocAdjunto) {
		this.carpetaDocAdjunto = carpetaDocAdjunto;
	}
		
}
