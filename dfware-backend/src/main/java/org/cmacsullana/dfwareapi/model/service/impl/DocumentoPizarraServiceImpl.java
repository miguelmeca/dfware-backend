package org.cmacsullana.dfwareapi.model.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

import org.cmacsullana.dfwareapi.constants.UtilConstants;
import org.cmacsullana.dfwareapi.excepciones.DocumentoException;
import org.cmacsullana.dfwareapi.model.dao.interfaces.IDocumentoPizarra;
import org.cmacsullana.dfwareapi.model.entity.DocumentoPizarra;
import org.cmacsullana.dfwareapi.model.service.interfaces.IDocumentoPizarraService;
import org.cmacsullana.dfwareapi.upload.UploadFiles;
import org.cmacsullana.dfwareapi.utilitarios.AppUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
@Transactional
public class DocumentoPizarraServiceImpl implements IDocumentoPizarraService {

	private final Path fileStorageLocation;
	
	@Autowired
	IDocumentoPizarra iDocumentoPizarraDao;

	@Autowired
	public DocumentoPizarraServiceImpl(UploadFiles upload) {
		this.fileStorageLocation = Paths
				.get(upload.getUploadDir())
				.toAbsolutePath()
				.normalize();
		try {
			Files.createDirectories(this.fileStorageLocation);
		} catch (Exception ex) {
			throw new DocumentoException(
					"No se pudo crear el directorio en donde se cargaran los archivos subidos.",
					ex
					);
		}

	}
	
	public String storeFile(MultipartFile file, DocumentoPizarra documento) { 

		//Normaliza el nombre del archivo que se sube
		String originalFileName = StringUtils.cleanPath(file.getOriginalFilename());
		String fileName = "";
		/*String codigoDocumento = AppUtil.generarCodigo("DOC", iDocumentoPizarraDao.count(),
				UtilConstants.LONGITUD_CORREL_COD_DOCUM_PIZARRA);
		documento.setCodigodocumento(codigoDocumento);*/

		try {
			// Revisa si el nombre del archivo contiene caracteres invalidos
			if (originalFileName.contains("..")) {
				throw new DocumentoException(
						"Atención! El nombre del archivo carácteres invalidos en la ruta " + originalFileName);
			}

			fileName = documento.getCodigodocumento() + "_"+originalFileName;

			// Copy file to the target location (Replacing existing file with the same name)

			Path targetLocation = this.fileStorageLocation.resolve(fileName);

			Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

			if (documento != null) {
				
				documento.setNombreDocumento(originalFileName);
				iDocumentoPizarraDao.save(documento);
			} 

			return fileName;

		} catch (IOException ex) {

			throw new DocumentoException("No se pudo almacenar el archivo " + fileName + ". Intentelo nuevamente!", ex);

		}

	}
	
	public Resource loadFileAsResource(String fileName) throws Exception {

		try {

			Path filePath = this.fileStorageLocation.resolve(fileName).normalize();

			Resource resource = new UrlResource(filePath.toUri());

			if (resource.exists()) {

				return resource;

			} else {

				throw new FileNotFoundException("No se encontró el archivo" + fileName);

			}

		} catch (MalformedURLException ex) {

			throw new FileNotFoundException("No se encontró el archivo " + fileName);

		}
	}
	
	@Override
	public List<DocumentoPizarra> getListDocumentosActivos() {

		return (List<DocumentoPizarra>) iDocumentoPizarraDao.findByIndicadorEstadoOrderByCodigodocumentoAsc("A");
	}

	@Override
	public DocumentoPizarra guardarDocumentoPizarra(DocumentoPizarra documento) {
		documento.setCodigodocumento(AppUtil.generarCodigo("DOC", iDocumentoPizarraDao.count(),
				UtilConstants.LONGITUD_CORREL_COD_DOCUM_PIZARRA));
		return iDocumentoPizarraDao.save(documento);
	}

	@Override
	public DocumentoPizarra actualizarDocumentoPizarra(DocumentoPizarra documento) {

		return iDocumentoPizarraDao.save(documento);
	}

	@Override
	public DocumentoPizarra obtenerDocumentoByCodigo(String codigoDocumento) {

		return iDocumentoPizarraDao.findById(codigoDocumento).orElse(null);
	}

	@Override
	public List<DocumentoPizarra> getListDocumentos() {
		return iDocumentoPizarraDao.findAllByOrderByCodigodocumentoAsc();
	}

	@Override
	public void eliminarArchivo(String nombreArchivo) {
		Path targetLocation = this.fileStorageLocation.resolve(nombreArchivo);
		File archivoAnterior = targetLocation.toFile();
		if(archivoAnterior.exists() && archivoAnterior.canRead()) {
			archivoAnterior.delete();
		}
		
	}

}
