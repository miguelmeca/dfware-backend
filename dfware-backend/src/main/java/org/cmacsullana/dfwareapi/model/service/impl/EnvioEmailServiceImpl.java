package org.cmacsullana.dfwareapi.model.service.impl;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.cmacsullana.dfwareapi.model.service.interfaces.IMailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class EnvioEmailServiceImpl implements IMailService{

	private final static Logger logger = LoggerFactory.getLogger(EnvioEmailServiceImpl.class);
	
	@Autowired
	private JavaMailSender mailSender;
	
	@Override
	public boolean enviarEmail(String emailDestino, String mensaje, String asunto) {

		boolean enviado=false;
		MimeMessage message = mailSender.createMimeMessage();
		MimeMessageHelper helper=new MimeMessageHelper(message);
		try {
			helper.setText(emailDestino);
			helper.setSubject(asunto);
			helper.setText(mensaje);
			mailSender.send(message);
			enviado=true;
			logger.info("el correo ha sido enviado con éxito");
		}catch (MessagingException e) {
			logger.error("Hubo problemas al enviar el correo. Detalle: "+e);
		}
		return enviado;
		
	}

}
