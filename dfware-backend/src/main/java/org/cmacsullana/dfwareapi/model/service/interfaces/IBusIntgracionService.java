package org.cmacsullana.dfwareapi.model.service.interfaces;

import org.cmacsullana.dfwareapi.bean.Usuario;

public interface IBusIntgracionService {

	public Usuario loginUsuario(String username, String clave);
		
}
