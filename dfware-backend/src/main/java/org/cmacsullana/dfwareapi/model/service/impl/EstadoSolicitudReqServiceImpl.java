package org.cmacsullana.dfwareapi.model.service.impl;

import java.util.List;
import java.util.Optional;

import org.cmacsullana.dfwareapi.model.dao.interfaces.IEstadoSolicitudRequisicionDao;
import org.cmacsullana.dfwareapi.model.entity.EstadoSolicitudRequisicion;
import org.cmacsullana.dfwareapi.model.service.interfaces.IEstadoSolicitudRequisicionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class EstadoSolicitudReqServiceImpl implements IEstadoSolicitudRequisicionService{

	@Autowired
	IEstadoSolicitudRequisicionDao iEstadoSolicReqDao;
	
	@Override
	@Transactional(readOnly=true)
	public List<EstadoSolicitudRequisicion> getListEstadoSolicitudes() {
		return (List<EstadoSolicitudRequisicion>)iEstadoSolicReqDao.findAll();
	}
	
	public Optional<EstadoSolicitudRequisicion> getEstadoByCodigo(String codigo) {
		return iEstadoSolicReqDao.findById(codigo);
	}

}
