package org.cmacsullana.dfwareapi.model.dao.interfaces;

import org.cmacsullana.dfwareapi.model.entity.SituacionSolicitudRequisicion;
import org.springframework.data.repository.CrudRepository;

public interface ISituacionRequisicionDao extends CrudRepository<SituacionSolicitudRequisicion, String>{

}
