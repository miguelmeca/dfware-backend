package org.cmacsullana.dfwareapi.model.dao.interfaces;

import org.cmacsullana.dfwareapi.model.entity.EstadoSolicitudRequisicion;
import org.springframework.data.repository.CrudRepository;

public interface IEstadoSolicitudRequisicionDao extends CrudRepository<EstadoSolicitudRequisicion, String>{

}
