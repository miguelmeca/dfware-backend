package org.cmacsullana.dfwareapi.model.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "PL_MAE_EST_SOLIC_REQ")
public class EstadoSolicitudRequisicion implements Serializable {
	
	@Id
	@Column(name = "COD_EST_SOLIC_REQ", length = 3)
	private String codigoEstadoSolicitud;
	
	@OneToMany
	@JoinColumn(name = "COD_EST_SOLIC_REQ")
	private List<SituacionSolicitudRequisicion> situacionSolicRequisicion;
	
	@Column(name = "NOMBRE_ESTADO")
	private String nombreEstadoSolicitud;
	
	@Column(name = "NUM_ORDEN_ESTADO")
	private Integer numeroOrdenEstado;
	
	@Column(name = "DESCRIP_DET_ESTADO")
	private String descripcionEstadoSolicitud;

	public EstadoSolicitudRequisicion() {

	}

	public String getCodigoEstadoSolicitud() {
		return codigoEstadoSolicitud;
	}

	public void setCodigoEstadoSolicitud(String codigoEstadoSolicitud) {
		this.codigoEstadoSolicitud = codigoEstadoSolicitud;
	}

	public String getNombreEstadoSolicitud() {
		return nombreEstadoSolicitud;
	}

	public void setNombreEstadoSolicitud(String nombreEstadoSolicitud) {
		this.nombreEstadoSolicitud = nombreEstadoSolicitud;
	}

	public Integer getNumeroOrdenEstado() {
		return numeroOrdenEstado;
	}

	public void setNumeroOrdenEstado(Integer numeroOrdenEstado) {
		this.numeroOrdenEstado = numeroOrdenEstado;
	}

	public String getDescripcionEstadoSolicitud() {
		return descripcionEstadoSolicitud;
	}

	public void setDescripcionEstadoSolicitud(String descripcionEstadoSolicitud) {
		this.descripcionEstadoSolicitud = descripcionEstadoSolicitud;
	}
		
	private static final long serialVersionUID = 1L;
	
}
