package org.cmacsullana.dfwareapi.model.service.interfaces;

public interface IMailService {

	public boolean enviarEmail(String emailDestino, String mensaje, String asunto);
}
