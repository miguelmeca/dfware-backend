package org.cmacsullana.dfwareapi.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="PL_MOD_DOCUMENTO_PIZARRA")
public class DocumentoPizarra implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="CODIGO_DOCUMENTO")
	private String codigodocumento;
	
	@Column(name="NOMBRE_DOCUMENTO")
	private String nombreDocumento;
	
	@Column(name="DESCR_DOCUMENTO")
	private String descripcionDocumento;
	
	@Column(name="TIPO_DOCUMENTO")
	private String tipoDocumento;
	
	@Column(name = "INDICA_ESTADO")
	private String indicadorEstado;
	
	@Column(name = "ADICIONADO_POR")
	private String adicionadoPor;
	
	@Column(name = "FECHA_ADICION")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaAdicion;
	
	@Column(name = "MODIFICADO_POR")
	private String modificadoPor;
	
	@Column(name = "FECHA_MODIFICACION")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaModificacion;
	
	@PrePersist
	public void prePersist() {
		indicadorEstado = "A";
		fechaAdicion = new Date();
	}
	
	@PreUpdate
	public void preUpdate() {
		fechaModificacion = new Date();
	}

	public String getCodigodocumento() {
		return codigodocumento;
	}

	public void setCodigodocumento(String codigodocumento) {
		this.codigodocumento = codigodocumento;
	}

	public String getNombreDocumento() {
		return nombreDocumento;
	}

	public void setNombreDocumento(String nombreDocumento) {
		this.nombreDocumento = nombreDocumento;
	}

	public String getDescripcionDocumento() {
		return descripcionDocumento;
	}

	public void setDescripcionDocumento(String descripcionDocumento) {
		this.descripcionDocumento = descripcionDocumento;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getIndicadorEstado() {
		return indicadorEstado;
	}

	public void setIndicadorEstado(String indicadorEstado) {
		this.indicadorEstado = indicadorEstado;
	}

	public String getAdicionadoPor() {
		return adicionadoPor;
	}

	public void setAdicionadoPor(String adicionadoPor) {
		this.adicionadoPor = adicionadoPor;
	}

	public Date getFechaAdicion() {
		return fechaAdicion;
	}

	public void setFechaAdicion(Date fechaAdicion) {
		this.fechaAdicion = fechaAdicion;
	}

	public String getModificadoPor() {
		return modificadoPor;
	}

	public void setModificadoPor(String modificadoPor) {
		this.modificadoPor = modificadoPor;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	/*public String getUploadDir() {
		return uploadDir;
	}

	public void setUploadDir(String uploadDir) {
		this.uploadDir = uploadDir;
	}*/
	
}
