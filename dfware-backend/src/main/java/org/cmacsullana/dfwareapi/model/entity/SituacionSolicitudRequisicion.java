package org.cmacsullana.dfwareapi.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "PL_MAE_SITUACION_SOLIC_REQ")
public class SituacionSolicitudRequisicion implements Serializable {

	@Id
	@Column(name = "COD_SITUACION_SOLIC_REQ")
	private String codigoSituacionSolicitudReq;
	
	@ManyToOne
	@JoinColumn(name = "COD_EST_SOLIC_REQ")
	private EstadoSolicitudRequisicion estadoSolicitudReq;
	
	@Column(name = "DESCRIP_DET_SITUACION")
	private String descripcionSituacion;

	public SituacionSolicitudRequisicion() {

	}
	
	public String getCodigoSituacionSolicitudReq() {
		return codigoSituacionSolicitudReq;
	}

	public void setCodigoSituacionSolicitudReq(String codigoSituacionSolicitudReq) {
		this.codigoSituacionSolicitudReq = codigoSituacionSolicitudReq;
	}

	
	public EstadoSolicitudRequisicion getEstadoSolicitudReq() {
		return estadoSolicitudReq;
	}

	public void setEstadoSolicitudReq(EstadoSolicitudRequisicion estadoSolicitudReq) {
		this.estadoSolicitudReq = estadoSolicitudReq;
	}

	public String getDescripcionSituacion() {
		return descripcionSituacion;
	}

	public void setDescripcionSituacion(String descripcionSituacion) {
		this.descripcionSituacion = descripcionSituacion;
	}

	private static final long serialVersionUID = 1L;

}
