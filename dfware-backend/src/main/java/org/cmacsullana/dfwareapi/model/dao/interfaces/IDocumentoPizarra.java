package org.cmacsullana.dfwareapi.model.dao.interfaces;

import java.util.List;

import org.cmacsullana.dfwareapi.model.entity.DocumentoPizarra;
import org.springframework.data.repository.CrudRepository;

public interface IDocumentoPizarra extends CrudRepository<DocumentoPizarra, String>{

	public List<DocumentoPizarra> findByIndicadorEstadoOrderByCodigodocumentoAsc(String indicadorEstado);
	public List<DocumentoPizarra> findAllByOrderByCodigodocumentoAsc();	
	
}
