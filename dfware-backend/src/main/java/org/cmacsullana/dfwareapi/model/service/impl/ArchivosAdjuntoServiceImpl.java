package org.cmacsullana.dfwareapi.model.service.impl;

import java.util.List;

import org.cmacsullana.dfwareapi.dto.ArchivoAdjuntoServicioDTO;
import org.cmacsullana.dfwareapi.model.dao.interfaces.IArchivosAdjuntosDao;
import org.cmacsullana.dfwareapi.model.service.interfaces.IArchivosAdjuntosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ArchivosAdjuntoServiceImpl implements IArchivosAdjuntosService{

	@Autowired
	private IArchivosAdjuntosDao iArchivoAdjDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<ArchivoAdjuntoServicioDTO> getListArchivosByCodigoServicio(String codigoServicio) {
		
		return iArchivoAdjDao.getListArchivosByCodigoServicio(codigoServicio);
	}

}
