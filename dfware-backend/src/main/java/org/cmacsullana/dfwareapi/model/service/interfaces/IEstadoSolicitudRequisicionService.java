package org.cmacsullana.dfwareapi.model.service.interfaces;

import java.util.List;
import java.util.Optional;

import org.cmacsullana.dfwareapi.model.entity.EstadoSolicitudRequisicion;

public interface IEstadoSolicitudRequisicionService {

	public List<EstadoSolicitudRequisicion> getListEstadoSolicitudes();
	public Optional<EstadoSolicitudRequisicion> getEstadoByCodigo(String codigo);
}
