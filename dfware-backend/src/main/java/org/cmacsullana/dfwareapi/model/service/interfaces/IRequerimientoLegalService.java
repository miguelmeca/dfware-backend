package org.cmacsullana.dfwareapi.model.service.interfaces;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.cmacsullana.dfwareapi.dto.BandejaSolcitudesTO;
import org.cmacsullana.dfwareapi.dto.HistorialSolicitudDTO;
import org.cmacsullana.dfwareapi.excepciones.BusinessException;
import org.cmacsullana.dfwareapi.model.entity.RequerimientoLegal;
import org.springframework.data.repository.query.Param;
import org.springframework.web.multipart.MultipartFile;

public interface IRequerimientoLegalService {

	public RequerimientoLegal grabarRequerimientoLegal(RequerimientoLegal requerimiento) throws BusinessException;
	
	public String directorioAlmacenamiento();
	
	public void almacenArchivos(List<MultipartFile>listaFiles, String directorio);
	
	public List<RequerimientoLegal> findByUsuRegTicketAndFechaRegTicket(String usuRegTicket, Date fechaRegTocket);
	
	public Map<String, Object> estadisticaByIndEstadoSolicAndFechaRegTicketAndUsuRegTicket(String usuRegTicket);
	
	public List<BandejaSolcitudesTO> listaBandejaSolcitudes(@Param("codigoUsuario") String codigoUsuario);
	
	public RequerimientoLegal detalleRequerimientoByCodTicket(String codTicket);
	
	void consultaDetalleSolicitud(String codTicket, String codUsuario) ;
	
	
	
}
