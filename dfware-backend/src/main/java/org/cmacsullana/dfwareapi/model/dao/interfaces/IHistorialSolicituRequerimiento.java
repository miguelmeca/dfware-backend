package org.cmacsullana.dfwareapi.model.dao.interfaces;

import java.util.List;

import org.cmacsullana.dfwareapi.dto.HistorialSolicitudDTO;
import org.cmacsullana.dfwareapi.model.entity.HistorialSolicitudRequerimiento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface IHistorialSolicituRequerimiento extends JpaRepository<HistorialSolicitudRequerimiento, String>{

	@Query("SELECT new org.cmacsullana.dfwareapi.dto.HistorialSolicitudDTO(h.fechaMovimiento,h.usuMovimiento, h.indEstadoSolic, "
			+ "(SELECT e.nombreEstadoSolicitud FROM EstadoSolicitudRequisicion e WHERE e.codigoEstadoSolicitud=h.indEstadoSolic) AS estado,"
			+ " h.indSituacSolic, (SELECT s.descripcionSituacion FROM SituacionSolicitudRequisicion s WHERE s.codigoSituacionSolicitudReq=h.indSituacSolic)"
			+ " AS situacion) FROM HistorialSolicitudRequerimiento h WHERE h.codTicketServicio=?1 ORDER BY h.fechaMovimiento")
	List<HistorialSolicitudDTO> listHistorialSolicitud(String numTicket);
	
}
