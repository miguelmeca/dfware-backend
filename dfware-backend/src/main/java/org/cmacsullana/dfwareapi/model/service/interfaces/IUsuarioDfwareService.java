package org.cmacsullana.dfwareapi.model.service.interfaces;

import org.cmacsullana.dfwareapi.model.entity.UsuarioDfware;

public interface IUsuarioDfwareService {

	public UsuarioDfware buscarUsuarioByCodigo(String codigoUsuario);
}
