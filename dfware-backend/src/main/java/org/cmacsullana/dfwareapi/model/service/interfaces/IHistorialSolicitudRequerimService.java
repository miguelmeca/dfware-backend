package org.cmacsullana.dfwareapi.model.service.interfaces;

import java.util.Date;
import java.util.List;

import org.cmacsullana.dfwareapi.dto.HistorialSolicitudDTO;


public interface IHistorialSolicitudRequerimService {

	public void grabarHistorialSolicitud(String codTicket, String anio, String mes, Date fecha, String usuario,
			String estRegistro, String sitRegistro, String motivo);
	
	List<HistorialSolicitudDTO> listHistorialSolicitud(String numTicket);
	
}
