package org.cmacsullana.dfwareapi.model.service.impl;

import java.util.Date;
import java.util.List;

import org.cmacsullana.dfwareapi.dto.HistorialSolicitudDTO;
import org.cmacsullana.dfwareapi.excepciones.BusinessException;
import org.cmacsullana.dfwareapi.excepciones.RequiredException;
import org.cmacsullana.dfwareapi.model.dao.interfaces.IHistorialSolicituRequerimiento;
import org.cmacsullana.dfwareapi.model.entity.HistorialSolicitudRequerimiento;
import org.cmacsullana.dfwareapi.model.service.interfaces.IHistorialSolicitudRequerimService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class HistorialSolicitudRequerServiceImpl implements IHistorialSolicitudRequerimService{

	@Autowired
	IHistorialSolicituRequerimiento iHistorial;
	
	@Override
	@Transactional(rollbackFor = {BusinessException.class, RequiredException.class})
	public void grabarHistorialSolicitud(String codTicket, String anio, String mes, Date fecha, String usuario,
			String estRegistro, String sitRegistro, String motivo) {
		
		HistorialSolicitudRequerimiento historial = new HistorialSolicitudRequerimiento();
		historial.setCodTicketServicio(codTicket);
		historial.setNumAnioRegistro(anio);
		historial.setMesAnioregistro(mes);
		historial.setFechaMovimiento(fecha);
		historial.setUsuMovimiento(usuario);
		historial.setIndEstadoSolic(estRegistro);
		historial.setIndSituacSolic(sitRegistro);
		historial.setMotivo(motivo);
		iHistorial.save(historial);
		
	}
	
	@Transactional(readOnly = true)
	public List<HistorialSolicitudDTO> listHistorialSolicitud(String numTicket){
		return iHistorial.listHistorialSolicitud(numTicket);
	}

}
