package org.cmacsullana.dfwareapi.model.service.impl;

import org.cmacsullana.dfwareapi.model.dao.interfaces.IUsuarioDfware;
import org.cmacsullana.dfwareapi.model.entity.UsuarioDfware;
import org.cmacsullana.dfwareapi.model.service.interfaces.IUsuarioDfwareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioDfwareServiceImpl implements IUsuarioDfwareService{

	@Autowired
	IUsuarioDfware usuarioDao;
	
	@Override
	public UsuarioDfware buscarUsuarioByCodigo(String codigoUsuario) {
		return usuarioDao.findById(codigoUsuario).orElse(null);
	}

}
