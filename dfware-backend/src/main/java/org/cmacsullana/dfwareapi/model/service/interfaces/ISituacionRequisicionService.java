package org.cmacsullana.dfwareapi.model.service.interfaces;

import java.util.List;

import org.cmacsullana.dfwareapi.model.entity.SituacionSolicitudRequisicion;

public interface ISituacionRequisicionService {

	public List<SituacionSolicitudRequisicion> getListSituacionRequisicion();
}
