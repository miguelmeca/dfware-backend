package org.cmacsullana.dfwareapi.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "PL_MOD_REQ_SOLICITUD")
public class RequerimientoLegal implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "COD_TICKET_SERVICIO")
	private String codTicketServicio;
	
	@Column(name = "NUM_ANIO_REGIS")
	private String numAnioRegistro;
	
	@Column(name = "MES_ANIO_REGIS")
	private String mesAnioregistro;
	
	@Column(name = "FECHA_REG_TICKET")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaRegTicket;
	
	@Column(name = "USU_REG_TICKET")
	private String usuRegTicket;
	
	@Column(name = "COD_SERV_LEGAL")
	private String codServLegal;
	
	@Column(name = "IND_ESTADO_SOLIC")
	private String indEstadoSolic;
	
	@Column(name = "IND_SITUAC_SOLIC")
	private String indSituacSolic;
	
	@Column(name = "RUTA_DIREC_ARCHIVO")
	private String rutaDirArchivo;
	
	@Column(name = "CLIENTE_ASUNTO")
	private String clienteAsunto;
	
	@Column(name = "DETALLE_SOLICITUD")
	private String detalleSolicitud;
	
	@Column(name = "TIPO_CREDITO")
	private String tipoCredito;
	
	@Column(name = "USO_BIEN")
	private String usoBien;
	
	@Column(name = "OFICINA_REGISTRAL")
	private String oficinaRegistral;
	
	@Column(name = "POLIZA_SEG_RIESGO")
	private String polizaSegRiesgo;
	
	@Column(name = "DIREC_DOMIC_CLIENTE")
	private String direcDomicCliente;
	
	@Column(name = "ESTADO_CIVIL")
	private String estadoCivil;
	
	@Column(name = "NOMBRE_NOTARIA")
	private String nombreNotaria;
	
	@Column(name = "MONTO_CREDITO")
	private Double montoCredito;
	
	@Column(name = "MON_MONTO_CREDOTO")
	private String monedaMontoCredito;
	
	@Column(name = "IMPORTE_ASUME_CMAC")
	private Double importeAsumeCmac;
	
	@Column(name = "MON_IMPORT_ASUME_CMAC")
	private String monedaImportAsumeCmac;
	
	@Column(name = "DIREC_DOMIC_VENDEDOR")
	private String direcDomicVendedor;
	
	@Column(name = "DIREC_DOMIC_COMPRADOR")
	private String direcDomicComprador;
	
	@Column(name = "DIREC_INMUEBLE")
	private String direcInmueble;
	
	@Column(name = "NOMBRE_FIADOR_SOLID")
	private String nombreFiadorSolid;
	
	@Column(name = "DIREC_DOMIC_FIADOR_SOLID")
	private String direcDomicFiadorSolid;
	
	@Column(name = "MONTO_EXP_TOTAL")
	private Double montoExpTotal;
	
	@Column(name = "MON_MONTO_EXP_TOTAL")
	private String monedaMontoExpTotal;
	
	@Column(name = "IMPORTE_BIEN")
	private Double importeBien;
	
	@Column(name = "MON_IMPORTE_BIEN")
	private String monedaImporteBien;
	
	@Column(name = "IMPORTE_GRAVAMEN")
	private Double importeGravamen;
	
	@Column(name = "MON_IMPORTE_GRAVAMEN")
	private String monedaImporteGravamen;
	
	@Column(name = "IMPORTE_VALOR_CONVENIO")
	private Double importeValorCovenio;
	
	@Column(name = "MON_IMPORT_VALOR_CONVENIO")
	private String monedaImportValorConvenio;
	
	@Column(name = "IMPORTE_ASUME_CLIENTE")
	private Double importeAsumeCliente;
	
	@Column(name = "MON_IMPORTE_ASUME_CLIENTE")
	private String monedaImporteAsumeCliente;
	
	@Column(name = "VALOR_COMERCIAL")
	private Double valorComercial;
	
	@Column(name = "MON_VALOR_COMERCIAL")
	private String monValorComercial;
	
	@Column(name = "MONTO_EXP_TOT_FIADOR_SOLID")
	private Double montoExpTotFiadorSolid;
	
	@Column(name = "MON_MONT_EXP_TOT_FIAD_SOLID")
	private String monedaMontExpTotFiadorSolid;
	
	@Column(name = "USU_DIR_TICKET")
	private String usuDirTicket;
	
	@Column(name = "USU_RES_TICKET")
	private String usuResTicket;
	
	@Column(name = "FEC_CONS_TICKET")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaConsTicket;
	
	@Column(name = "PLAZO_MESES")
	private String plazoMeses;
	
	@Column(name = "PORC_COMPENSATORIO")
	private Double porcCompensatorio;
	
	@Column(name = "PORC_MORATORIO")
	private Double porcMoratorio;
	
	@Column(name = "RAZON_SOCIAL")
	private String razonSocial;
	
	@Column(name = "RUC_PERSONA_JURIDICA")
	private String rucPersonaJuridica;
	
	@Column(name = "DOMICILIO_PERS_JURIDICA")
	private String domicilioPersJuridica;
	
	@Column(name = "FECHA_INICIO_CONVENIO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaInicioConvenio;
	
	@Column(name = "FECHA_FIN_CONVENIO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaFinConvenio;
	
	@Column(name = "CUENTA_MORA_PERS_JURIDICA")
	private String cuentaMoraPersJuridica;
	
	@Column(name = "INDICADOR_VIVIENDA")
	private String indicadorVivienda;
	
	@Column(name = "ESTADO_INMUEBLE")
	private String estadoInmueble;
	
	@Column(name = "DESTINO_CREDITO")
	private String destinoCredito;
	
	@Column(name = "NOMBRE_PROYECTO")
	private String nombreProyecto;
	
	@Column(name = "FECHA_FIN_PROYECTO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaFinProyecto;
	
	@Column(name = "MOTIVO_NO_INSCRIPCION")
	private String motivoNoInscripcion;
	
	@Column(name = "GARANTIA_ACTUAL")
	private Double garantiaActual;
	
	@Column(name = "NOMBRE_OBLIGADO_PRINCIPAL")
	private String nombreObligadoPrincipal;
	
	@Column(name = "UBICACION_BIEN_MUEBLE")
	private String ubicacionBienMueble;
	
	@Column(name = "NOMBRE_DEPOSITARIO")
	private String nombreDepositario;
	
	@Column(name = "DOMICILIO_CONSERV_BIEN")
	private String domicilioConservBien;
	
	@Column(name = "ADJ_SOLIC_CRED_APROBADA")
	private String adjSolicCredAprobada;
	
	@Column(name = "ADJ_TEST_ESCRIT_PUBLICA")
	private String adjTestEscritPublica;
	
	@Column(name = "ADJ_DNI_CLIENTE")
	private String adjDniCliente;
	
	@Column(name = "ADJ_TASACION_BIEN")
	private String adjTasacionBien;
	
	@Column(name = "ADJ_PARTIDA_COMPL_ACTUALIZ")
	private String adjPartidaComplActualiz;
	
	@Column(name = "ADJ_DNI_COMPRADOR")
	private String adjDniComprador;
	
	@Column(name = "ADJ_DNI_VENDEDOR")
	private String adjDniVendedor;
	
	@Column(name = "ADJ_DNI_REPRES_LEGAL")
	private String adjDniRepresLegal;
	
	@Column(name = "ADJ_DNI_FIADOR_SOLIDARIO")
	private String adjDnifiadorSolidario;
	
	@Column(name = "ADJ_DNI_OBLIGADO_PRINCIPAL")
	private String adjDniObligadoPrincipal;
	
	@Column(name = "ADJ_FICHA_REGISTRAL")
	private String adjFichaRegistral;
	
	@Column(name = "ADJ_BOLETA_INFORMATIVA_ACT")
	private String adjBoletaInformativaAct;
	
	@Column(name = "ADJ_COPIA_LITERAL_INMUEBLE")
	private String adjCopiaLiteralInmueble;
	
	@Column(name = "ADJ_CERTIF_DEPOSITO")
	private String adjCertifDeposito;
	
	@Column(name = "ADJ_CONTR_CRED_HIPOTECARIO")
	private String adjContrCredHipotecario;
	
	@Column(name = "ADJ_CONTR_COMPRA_VENTA")
	private String adjContrCompraVenta;
	
	@Column(name = "ADJ_SOLICITUD")
	private String adjSolicitud;
	
	@Column(name = "ADJ_VIGENCIA_PODER")
	private String adjVigenciaPoder;
	
	@Column(name = "ADJ_POSICION_CLIENTE")
	private String adjPosicionCliente;
	
	@Column(name = "ADJ_CONTR_GARANT_MOBILIARIA")
	private String adjContrGarantMobiliaria;
	
	@Column(name = "ADJ_DECL_UNICA_ADUANA")
	private String adjDeclUnicaAduana;
	
	@Column(name = "ADJ_PROFORMA")
	private String adjProforma;
	
	@Column(name = "ADJ_CARTA_SIMPLE")
	private String adjCartaSimple;
	
	@Column(name = "ADJ_PODER_FUERA_REG_CLIENTE")
	private String adjPoderFueraRegCliente;
	
	@Column(name = "ADJ_PODER_INSC_REG_PUB_CLIEN")
	private String adjPoderInscRegPubCliente;
	
	@Column(name = "NOMBRE_ADJ_01")
	private String nombreAdj01;
	
	@Column(name = "NOMBRE_ADJ_02")
	private String nombreAdj02;
	
	@Column(name = "NOMBRE_ADJ_03")
	private String nombreAdj03;
	
	@Column(name = "ADJ_OTRO_ARCHIVO_01")
	private String adjOtroArchivo01;
	
	@Column(name = "ADJ_OTRO_ARCHIVO_02")
	private String adjOtroArchivo02;
	
	@Column(name = "ADJ_OTRO_ARCHIVO_03")
	private String adjOtroArchivo03;
	
	@Column(name = "NUM_SOLIC_WORKFLOW")
	private String numeroSolicWorkflow;
	
	public String getCodTicketServicio() {
		return codTicketServicio;
	}

	public void setCodTicketServicio(String codTicketServicio) {
		this.codTicketServicio = codTicketServicio;
	}

	public String getNumAnioRegistro() {
		return numAnioRegistro;
	}

	public void setNumAnioRegistro(String numAnioRegistro) {
		this.numAnioRegistro = numAnioRegistro;
	}

	public String getMesAnioregistro() {
		return mesAnioregistro;
	}

	public void setMesAnioregistro(String mesAnioregistro) {
		this.mesAnioregistro = mesAnioregistro;
	}

	public Date getFechaRegTicket() {
		return fechaRegTicket;
	}

	public void setFechaRegTicket(Date fechaRegTicket) {
		this.fechaRegTicket = fechaRegTicket;
	}

	public String getUsuRegTicket() {
		return usuRegTicket;
	}

	public void setUsuRegTicket(String usuRegTicket) {
		this.usuRegTicket = usuRegTicket;
	}

	public String getCodServLegal() {
		return codServLegal;
	}

	public void setCodServLegal(String codServLegal) {
		this.codServLegal = codServLegal;
	}

	public String getIndEstadoSolic() {
		return indEstadoSolic;
	}

	public void setIndEstadoSolic(String indEstadoSolic) {
		this.indEstadoSolic = indEstadoSolic;
	}

	public String getIndSituacSolic() {
		return indSituacSolic;
	}

	public void setIndSituacSolic(String indSituacSolic) {
		this.indSituacSolic = indSituacSolic;
	}

	public String getRutaDirArchivo() {
		return rutaDirArchivo;
	}

	public void setRutaDirArchivo(String rutaDirArchivo) {
		this.rutaDirArchivo = rutaDirArchivo;
	}

	public String getClienteAsunto() {
		return clienteAsunto;
	}

	public void setClienteAsunto(String clienteAsunto) {
		this.clienteAsunto = clienteAsunto;
	}

	public String getDetalleSolicitud() {
		return detalleSolicitud;
	}

	public void setDetalleSolicitud(String detalleSolicitud) {
		this.detalleSolicitud = detalleSolicitud;
	}

	public String getTipoCredito() {
		return tipoCredito;
	}

	public void setTipoCredito(String tipoCredito) {
		this.tipoCredito = tipoCredito;
	}

	public String getUsoBien() {
		return usoBien;
	}

	public void setUsoBien(String usoBien) {
		this.usoBien = usoBien;
	}

	public String getOficinaRegistral() {
		return oficinaRegistral;
	}

	public void setOficinaRegistral(String oficinaRegistral) {
		this.oficinaRegistral = oficinaRegistral;
	}

	public String getPolizaSegRiesgo() {
		return polizaSegRiesgo;
	}

	public void setPolizaSegRiesgo(String polizaSegRiesgo) {
		this.polizaSegRiesgo = polizaSegRiesgo;
	}

	public String getDirecDomicCliente() {
		return direcDomicCliente;
	}

	public void setDirecDomicCliente(String direcDomicCliente) {
		this.direcDomicCliente = direcDomicCliente;
	}

	public String getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public String getNombreNotaria() {
		return nombreNotaria;
	}

	public void setNombreNotaria(String nombreNotaria) {
		this.nombreNotaria = nombreNotaria;
	}

	public Double getMontoCredito() {
		return montoCredito;
	}

	public void setMontoCredito(Double montoCredito) {
		this.montoCredito = montoCredito;
	}

	public String getMonedaMontoCredito() {
		return monedaMontoCredito;
	}

	public void setMonedaMontoCredito(String monedaMontoCredito) {
		this.monedaMontoCredito = monedaMontoCredito;
	}

	public Double getImporteAsumeCmac() {
		return importeAsumeCmac;
	}

	public void setImporteAsumeCmac(Double importeAsumeCmac) {
		this.importeAsumeCmac = importeAsumeCmac;
	}

	public String getMonedaImportAsumeCmac() {
		return monedaImportAsumeCmac;
	}

	public void setMonedaImportAsumeCmac(String monedaImportAsumeCmac) {
		this.monedaImportAsumeCmac = monedaImportAsumeCmac;
	}

	public String getDirecDomicVendedor() {
		return direcDomicVendedor;
	}

	public void setDirecDomicVendedor(String direcDomicVendedor) {
		this.direcDomicVendedor = direcDomicVendedor;
	}

	public String getDirecDomicComprador() {
		return direcDomicComprador;
	}

	public void setDirecDomicComprador(String direcDomicComprador) {
		this.direcDomicComprador = direcDomicComprador;
	}

	public String getDirecInmueble() {
		return direcInmueble;
	}

	public void setDirecInmueble(String direcInmueble) {
		this.direcInmueble = direcInmueble;
	}

	public String getNombreFiadorSolid() {
		return nombreFiadorSolid;
	}

	public void setNombreFiadorSolid(String nombreFiadorSolid) {
		this.nombreFiadorSolid = nombreFiadorSolid;
	}

	public String getDirecDomicFiadorSolid() {
		return direcDomicFiadorSolid;
	}

	public void setDirecDomicFiadorSolid(String direcDomicFiadorSolid) {
		this.direcDomicFiadorSolid = direcDomicFiadorSolid;
	}

	public Double getMontoExpTotal() {
		return montoExpTotal;
	}

	public void setMontoExpTotal(Double montoExpTotal) {
		this.montoExpTotal = montoExpTotal;
	}

	public String getMonedaMontoExpTotal() {
		return monedaMontoExpTotal;
	}

	public void setMonedaMontoExpTotal(String monedaMontoExpTotal) {
		this.monedaMontoExpTotal = monedaMontoExpTotal;
	}

	public Double getImporteBien() {
		return importeBien;
	}

	public void setImporteBien(Double importeBien) {
		this.importeBien = importeBien;
	}

	public String getMonedaImporteBien() {
		return monedaImporteBien;
	}

	public void setMonedaImporteBien(String monedaImporteBien) {
		this.monedaImporteBien = monedaImporteBien;
	}

	public Double getImporteGravamen() {
		return importeGravamen;
	}

	public void setImporteGravamen(Double importeGravamen) {
		this.importeGravamen = importeGravamen;
	}

	public String getMonedaImporteGravamen() {
		return monedaImporteGravamen;
	}

	public void setMonedaImporteGravamen(String monedaImporteGravamen) {
		this.monedaImporteGravamen = monedaImporteGravamen;
	}

	public Double getImporteValorCovenio() {
		return importeValorCovenio;
	}

	public void setImporteValorCovenio(Double importeValorCovenio) {
		this.importeValorCovenio = importeValorCovenio;
	}

	public String getMonedaImportValorConvenio() {
		return monedaImportValorConvenio;
	}

	public void setMonedaImportValorConvenio(String monedaImportValorConvenio) {
		this.monedaImportValorConvenio = monedaImportValorConvenio;
	}

	public Double getImporteAsumeCliente() {
		return importeAsumeCliente;
	}

	public void setImporteAsumeCliente(Double importeAsumeCliente) {
		this.importeAsumeCliente = importeAsumeCliente;
	}

	public String getMonedaImporteAsumeCliente() {
		return monedaImporteAsumeCliente;
	}

	public void setMonedaImporteAsumeCliente(String monedaImporteAsumeCliente) {
		this.monedaImporteAsumeCliente = monedaImporteAsumeCliente;
	}

	public String getMonValorComercial() {
		return monValorComercial;
	}

	public void setMonValorComercial(String monValorComercial) {
		this.monValorComercial = monValorComercial;
	}

	public Double getValorComercial() {
		return valorComercial;
	}

	public void setValorComercial(Double valorComercial) {
		this.valorComercial = valorComercial;
	}

	public Double getMontoExpTotFiadorSolid() {
		return montoExpTotFiadorSolid;
	}

	public void setMontoExpTotFiadorSolid(Double montoExpTotFiadorSolid) {
		this.montoExpTotFiadorSolid = montoExpTotFiadorSolid;
	}

	public String getMonedaMontExpTotFiadorSolid() {
		return monedaMontExpTotFiadorSolid;
	}

	public void setMonedaMontExpTotFiadorSolid(String monedaMontExpTotFiadorSolid) {
		this.monedaMontExpTotFiadorSolid = monedaMontExpTotFiadorSolid;
	}

	public String getUsuDirTicket() {
		return usuDirTicket;
	}

	public void setUsuDirTicket(String usuDirTicket) {
		this.usuDirTicket = usuDirTicket;
	}

	public String getUsuResTicket() {
		return usuResTicket;
	}

	public void setUsuResTicket(String usuResTicket) {
		this.usuResTicket = usuResTicket;
	}

	public Date getFechaConsTicket() {
		return fechaConsTicket;
	}

	public void setFechaConsTicket(Date fechaConsTicket) {
		this.fechaConsTicket = fechaConsTicket;
	}

	public String getPlazoMeses() {
		return plazoMeses;
	}

	public void setPlazoMeses(String plazoMeses) {
		this.plazoMeses = plazoMeses;
	}

	public Double getPorcCompensatorio() {
		return porcCompensatorio;
	}

	public void setPorcCompensatorio(Double porcCompensatorio) {
		this.porcCompensatorio = porcCompensatorio;
	}

	public Double getPorcMoratorio() {
		return porcMoratorio;
	}

	public void setPorcMoratorio(Double porcMoratorio) {
		this.porcMoratorio = porcMoratorio;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getRucPersonaJuridica() {
		return rucPersonaJuridica;
	}

	public void setRucPersonaJuridica(String rucPersonaJuridica) {
		this.rucPersonaJuridica = rucPersonaJuridica;
	}

	public String getDomicilioPersJuridica() {
		return domicilioPersJuridica;
	}

	public void setDomicilioPersJuridica(String domicilioPersJuridica) {
		this.domicilioPersJuridica = domicilioPersJuridica;
	}

	public Date getFechaInicioConvenio() {
		return fechaInicioConvenio;
	}

	public void setFechaInicioConvenio(Date fechaInicioConvenio) {
		this.fechaInicioConvenio = fechaInicioConvenio;
	}

	public Date getFechaFinConvenio() {
		return fechaFinConvenio;
	}

	public void setFechaFinConvenio(Date fechaFinConvenio) {
		this.fechaFinConvenio = fechaFinConvenio;
	}

	public String getCuentaMoraPersJuridica() {
		return cuentaMoraPersJuridica;
	}

	public void setCuentaMoraPersJuridica(String cuentaMoraPersJuridica) {
		this.cuentaMoraPersJuridica = cuentaMoraPersJuridica;
	}

	public String getIndicadorVivienda() {
		return indicadorVivienda;
	}

	public void setIndicadorVivienda(String indicadorVivienda) {
		this.indicadorVivienda = indicadorVivienda;
	}

	public String getEstadoInmueble() {
		return estadoInmueble;
	}

	public void setEstadoInmueble(String estadoInmueble) {
		this.estadoInmueble = estadoInmueble;
	}

	public String getDestinoCredito() {
		return destinoCredito;
	}

	public void setDestinoCredito(String destinoCredito) {
		this.destinoCredito = destinoCredito;
	}

	public String getNombreProyecto() {
		return nombreProyecto;
	}

	public void setNombreProyecto(String nombreProyecto) {
		this.nombreProyecto = nombreProyecto;
	}

	public Date getFechaFinProyecto() {
		return fechaFinProyecto;
	}

	public void setFechaFinProyecto(Date fechaFinProyecto) {
		this.fechaFinProyecto = fechaFinProyecto;
	}

	public String getMotivoNoInscripcion() {
		return motivoNoInscripcion;
	}

	public void setMotivoNoInscripcion(String motivoNoInscripcion) {
		this.motivoNoInscripcion = motivoNoInscripcion;
	}

	public Double getGarantiaActual() {
		return garantiaActual;
	}

	public void setGarantiaActual(Double garantiaActual) {
		this.garantiaActual = garantiaActual;
	}

	public String getNombreObligadoPrincipal() {
		return nombreObligadoPrincipal;
	}

	public void setNombreObligadoPrincipal(String nombreObligadoPrincipal) {
		this.nombreObligadoPrincipal = nombreObligadoPrincipal;
	}

	public String getUbicacionBienMueble() {
		return ubicacionBienMueble;
	}

	public void setUbicacionBienMueble(String ubicacionBienMueble) {
		this.ubicacionBienMueble = ubicacionBienMueble;
	}

	public String getNombreDepositario() {
		return nombreDepositario;
	}

	public void setNombreDepositario(String nombreDepositario) {
		this.nombreDepositario = nombreDepositario;
	}

	public String getDomicilioConservBien() {
		return domicilioConservBien;
	}

	public void setDomicilioConservBien(String domicilioConservBien) {
		this.domicilioConservBien = domicilioConservBien;
	}

	public String getAdjSolicCredAprobada() {
		return adjSolicCredAprobada;
	}

	public void setAdjSolicCredAprobada(String adjSolicCredAprobada) {
		this.adjSolicCredAprobada = adjSolicCredAprobada;
	}

	public String getAdjTestEscritPublica() {
		return adjTestEscritPublica;
	}

	public void setAdjTestEscritPublica(String adjTestEscritPublica) {
		this.adjTestEscritPublica = adjTestEscritPublica;
	}

	public String getAdjDniCliente() {
		return adjDniCliente;
	}

	public void setAdjDniCliente(String adjDniCliente) {
		this.adjDniCliente = adjDniCliente;
	}

	public String getAdjTasacionBien() {
		return adjTasacionBien;
	}

	public void setAdjTasacionBien(String adjTasacionBien) {
		this.adjTasacionBien = adjTasacionBien;
	}

	public String getAdjPartidaComplActualiz() {
		return adjPartidaComplActualiz;
	}

	public void setAdjPartidaComplActualiz(String adjPartidaComplActualiz) {
		this.adjPartidaComplActualiz = adjPartidaComplActualiz;
	}

	public String getAdjDniComprador() {
		return adjDniComprador;
	}

	public void setAdjDniComprador(String adjDniComprador) {
		this.adjDniComprador = adjDniComprador;
	}

	public String getAdjDniVendedor() {
		return adjDniVendedor;
	}

	public void setAdjDniVendedor(String adjDniVendedor) {
		this.adjDniVendedor = adjDniVendedor;
	}

	public String getAdjDniRepresLegal() {
		return adjDniRepresLegal;
	}

	public void setAdjDniRepresLegal(String adjDniRepresLegal) {
		this.adjDniRepresLegal = adjDniRepresLegal;
	}

	public String getAdjDnifiadorSolidario() {
		return adjDnifiadorSolidario;
	}

	public void setAdjDnifiadorSolidario(String adjDnifiadorSolidario) {
		this.adjDnifiadorSolidario = adjDnifiadorSolidario;
	}

	public String getAdjDniObligadoPrincipal() {
		return adjDniObligadoPrincipal;
	}

	public void setAdjDniObligadoPrincipal(String adjDniObligadoPrincipal) {
		this.adjDniObligadoPrincipal = adjDniObligadoPrincipal;
	}

	public String getAdjFichaRegistral() {
		return adjFichaRegistral;
	}

	public void setAdjFichaRegistral(String adjFichaRegistral) {
		this.adjFichaRegistral = adjFichaRegistral;
	}

	public String getAdjBoletaInformativaAct() {
		return adjBoletaInformativaAct;
	}

	public void setAdjBoletaInformativaAct(String adjBoletaInformativaAct) {
		this.adjBoletaInformativaAct = adjBoletaInformativaAct;
	}

	public String getAdjCopiaLiteralInmueble() {
		return adjCopiaLiteralInmueble;
	}

	public void setAdjCopiaLiteralInmueble(String adjCopiaLiteralInmueble) {
		this.adjCopiaLiteralInmueble = adjCopiaLiteralInmueble;
	}

	public String getAdjCertifDeposito() {
		return adjCertifDeposito;
	}

	public void setAdjCertifDeposito(String adjCertifDeposito) {
		this.adjCertifDeposito = adjCertifDeposito;
	}

	public String getAdjContrCredHipotecario() {
		return adjContrCredHipotecario;
	}

	public void setAdjContrCredHipotecario(String adjContrCredHipotecario) {
		this.adjContrCredHipotecario = adjContrCredHipotecario;
	}

	public String getAdjContrCompraVenta() {
		return adjContrCompraVenta;
	}

	public void setAdjContrCompraVenta(String adjContrCompraVenta) {
		this.adjContrCompraVenta = adjContrCompraVenta;
	}

	public String getAdjSolicitud() {
		return adjSolicitud;
	}

	public void setAdjSolicitud(String adjSolicitud) {
		this.adjSolicitud = adjSolicitud;
	}

	public String getAdjVigenciaPoder() {
		return adjVigenciaPoder;
	}

	public void setAdjVigenciaPoder(String adjVigenciaPoder) {
		this.adjVigenciaPoder = adjVigenciaPoder;
	}

	public String getAdjPosicionCliente() {
		return adjPosicionCliente;
	}

	public void setAdjPosicionCliente(String adjPosicionCliente) {
		this.adjPosicionCliente = adjPosicionCliente;
	}

	public String getAdjContrGarantMobiliaria() {
		return adjContrGarantMobiliaria;
	}

	public void setAdjContrGarantMobiliaria(String adjContrGarantMobiliaria) {
		this.adjContrGarantMobiliaria = adjContrGarantMobiliaria;
	}

	public String getAdjDeclUnicaAduana() {
		return adjDeclUnicaAduana;
	}

	public void setAdjDeclUnicaAduana(String adjDeclUnicaAduana) {
		this.adjDeclUnicaAduana = adjDeclUnicaAduana;
	}

	public String getAdjProforma() {
		return adjProforma;
	}

	public void setAdjProforma(String adjProforma) {
		this.adjProforma = adjProforma;
	}

	public String getAdjCartaSimple() {
		return adjCartaSimple;
	}

	public void setAdjCartaSimple(String adjCartaSimple) {
		this.adjCartaSimple = adjCartaSimple;
	}

	public String getAdjPoderFueraRegCliente() {
		return adjPoderFueraRegCliente;
	}

	public void setAdjPoderFueraRegCliente(String adjPoderFueraRegCliente) {
		this.adjPoderFueraRegCliente = adjPoderFueraRegCliente;
	}

	public String getAdjPoderInscRegPubCliente() {
		return adjPoderInscRegPubCliente;
	}

	public void setAdjPoderInscRegPubCliente(String adjPoderInscRegPubCliente) {
		this.adjPoderInscRegPubCliente = adjPoderInscRegPubCliente;
	}

	public String getNombreAdj01() {
		return nombreAdj01;
	}

	public void setNombreAdj01(String nombreAdj01) {
		this.nombreAdj01 = nombreAdj01;
	}

	public String getNombreAdj02() {
		return nombreAdj02;
	}

	public void setNombreAdj02(String nombreAdj02) {
		this.nombreAdj02 = nombreAdj02;
	}

	public String getNombreAdj03() {
		return nombreAdj03;
	}

	public void setNombreAdj03(String nombreAdj03) {
		this.nombreAdj03 = nombreAdj03;
	}

	public String getAdjOtroArchivo01() {
		return adjOtroArchivo01;
	}

	public void setAdjOtroArchivo01(String adjOtroArchivo01) {
		this.adjOtroArchivo01 = adjOtroArchivo01;
	}

	public String getAdjOtroArchivo02() {
		return adjOtroArchivo02;
	}

	public void setAdjOtroArchivo02(String adjOtroArchivo02) {
		this.adjOtroArchivo02 = adjOtroArchivo02;
	}

	public String getAdjOtroArchivo03() {
		return adjOtroArchivo03;
	}

	public void setAdjOtroArchivo03(String adjOtroArchivo03) {
		this.adjOtroArchivo03 = adjOtroArchivo03;
	}

	public String getNumeroSolicWorkflow() {
		return numeroSolicWorkflow;
	}

	public void setNumeroSolicWorkflow(String numeroSolicWorkflow) {
		this.numeroSolicWorkflow = numeroSolicWorkflow;
	}

}
