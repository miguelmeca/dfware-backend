package org.cmacsullana.dfwareapi.model.service.impl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cmacsullana.dfwareapi.bean.Role;
import org.cmacsullana.dfwareapi.bean.Usuario;
import org.cmacsullana.dfwareapi.busIntegracion.BusIntegracionClient;
import org.cmacsullana.dfwareapi.enums.ServiceLayerOperation;
import org.cmacsullana.dfwareapi.model.service.interfaces.IBusIntgracionService;
import org.cmacsullana.dfwareapi.utilitarios.AppUtil;
import org.cmacsullana.dfwareapi.utilitarios.ConstantesApp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class BusIntegracionServiceImpl implements IBusIntgracionService{

	@Autowired
	BusIntegracionClient busClient;
	
	@Value("${bus.accessChanel}")
	private String accessChanel;

	@Value("${bus.accesType}")
	private String accesType;
	
	@Value("${bus.accessKey}")
	private String accessKey;
	
	@Value("${app.validate.bus}")
	private Boolean validarServicioBus;
	
	@SuppressWarnings("unchecked")
	public Usuario loginUsuario(String userName, String clave) {
		ServiceLayerOperation serviceLayerOperation = ServiceLayerOperation.HELPDESK_VALIDAR_USUARIO_CORE;
		Map<String, Object> vars = new HashMap<String, Object>();
		Boolean servicioOK = false;
		Usuario user = new Usuario();
		
		AppUtil.addDefaultParams(vars, serviceLayerOperation);
		vars.put(ConstantesApp.VI_USUARIO, userName);
		vars.put(ConstantesApp.VI_PASSWORD, clave);
		vars.put(ConstantesApp.VI_TIPO, accesType);
		vars.put(ConstantesApp.VI_CANAL, this.accessChanel);
		
		Map<String, Object> result =busClient.getResult(vars);
		
		user.setPassword(clave);
		user.setCodigoAgencia((String)result.get(ConstantesApp.VO_CODIGO_AGENCIA));
		user.setUsuario((String)result.get(ConstantesApp.VI_USUARIO));
		user.setStatus((String)result.get(ConstantesApp.VO_STATUS));
		user.setNombre((String) result.get(ConstantesApp.VO_NOMBRE_USUARIO));
		user.setCodigoPerfil((String)result.get(ConstantesApp.VI_CODIGO_PERFIL));
		user.setCargo((String)result.get(ConstantesApp.VI_CARGO));
		user.setMensaje((String)result.get(ConstantesApp.VO_MESSAGE));
		user.setPerfil((String)result.get(ConstantesApp.VI_PERFIL));
		user.setCodigoCargo((String)result.get(ConstantesApp.VI_CODIGO_CARGO));
		user.setNombreAgencia((String)result.get(ConstantesApp.VO_AGENCIA));
		user.setCodError((String) result.get(ConstantesApp.VO_STATUS));
		user.setMsjError((String) result.get(ConstantesApp.VO_MESSAGE));
		
		servicioOK = user.getCodError().equals("00") ? true : false;
		
		if(servicioOK) {
			Role role;
			String tipoRol;
			Set<Role> myRoles = new HashSet<Role>();
			
			List<Map<String, Object>> listaRoles = (List<Map<String, Object>>) result.get(ConstantesApp.VO_CURSOR);
			for (Map<String, Object> item : listaRoles) {
				role = new Role();
				tipoRol = item.get("TIPO").toString();
				if (tipoRol.equals("A")) {
					role.setDescription(item.get("ROLE") != null ? item.get("ROLE").toString() : "");
					myRoles.add(role);
				}
			}
			user.setMyRoles(myRoles);
		}
		return user;
	}
	
}
