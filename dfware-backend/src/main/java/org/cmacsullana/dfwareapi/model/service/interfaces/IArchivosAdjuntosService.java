package org.cmacsullana.dfwareapi.model.service.interfaces;

import java.util.List;

import org.cmacsullana.dfwareapi.dto.ArchivoAdjuntoServicioDTO;

public interface IArchivosAdjuntosService {

	public List<ArchivoAdjuntoServicioDTO> getListArchivosByCodigoServicio(String codigoServicio);
}
