package org.cmacsullana.dfwareapi;

import org.cmacsullana.dfwareapi.upload.UploadFiles;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@EnableConfigurationProperties(UploadFiles.class)
@SpringBootApplication
public class DfwareBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(DfwareBackendApplication.class, args);
	}

}
