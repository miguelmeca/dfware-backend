package org.cmacsullana.dfwareapi.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.cmacsullana.dfwareapi.excepciones.BusinessException;
import org.cmacsullana.dfwareapi.model.entity.DocumentoPizarra;
import org.cmacsullana.dfwareapi.model.service.interfaces.IDocumentoPizarraService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/dfware")
public class DocumentoPizarraController {

	@Autowired
	IDocumentoPizarraService iDocumentoService;
	
	private static final Logger logger = LoggerFactory.getLogger(DocumentoPizarraController.class);
	
	ObjectMapper objectMapper = new ObjectMapper();
	
	@GetMapping("/documentosPizarra")
	public List<DocumentoPizarra> getListDocumentosActivos(){
		return iDocumentoService.getListDocumentos();
	}
	
	@PostMapping("/registrar-documPizarra")
	public ResponseEntity<?> grabarDocumentoPizarra(@RequestBody DocumentoPizarra documento) throws IOException{
		
		DocumentoPizarra documentoNuevo = null;
		Map<String, Object> response = new HashMap<>();
		
		try {
			documentoNuevo = iDocumentoService.guardarDocumentoPizarra(documento);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al grabar el registro en la base de datos");
			response.put("error: ", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje", "El documento a mostrar en la pizarra ha sido creado con éxito!!!");
		response.put("documento", documentoNuevo);
						
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
		
	}
	
	@PostMapping("/uploadDocPizarra")
	public ResponseEntity<?> uploadDocumentoPizarra(
			@RequestParam("file") MultipartFile file, 
			@RequestParam("codigoDocumento") String codigoDocumento) throws IOException{
		
		Map<String, Object> response = new HashMap<>();
		DocumentoPizarra documentoPizarra = iDocumentoService.obtenerDocumentoByCodigo(codigoDocumento);
		System.out.println("el codigo del documento:"+documentoPizarra.getCodigodocumento());
		String nombreArchivoAnterior = documentoPizarra.getNombreDocumento();
		if(nombreArchivoAnterior != null && nombreArchivoAnterior.length()>0) {
			iDocumentoService.eliminarArchivo(nombreArchivoAnterior);
		}
		
		String nombreArchivo = iDocumentoService.storeFile(file, documentoPizarra);
		String uriDescargarArchivo = ServletUriComponentsBuilder
				.fromCurrentContextPath()
				.path("/downloadFile")
				.path(nombreArchivo)
				.toUriString();
		response.put("urlDescarga", uriDescargarArchivo);
		response.put("mensaje", "El documento ha sido almacenado con éxito");
		
		return new ResponseEntity<Map<String, Object>>(response,HttpStatus.CREATED);
	}
	
	@GetMapping("/downloadFile/{codigoDocumento}")
	public ResponseEntity<Resource> downloadFile(@PathVariable String codigoDocumento, HttpServletRequest request) {

		DocumentoPizarra documento = iDocumentoService.obtenerDocumentoByCodigo(codigoDocumento);
		String fileName = documento.getCodigodocumento()+"_"+documento.getNombreDocumento();
		System.out.println("NOMBRE ARCHIVO:"+fileName);

		Resource resource = null;

		if (fileName != null && !fileName.isEmpty()) {

			try {
				resource = iDocumentoService.loadFileAsResource(fileName);
				System.out.println("RECURSO:"+resource);
			} catch (Exception e) {
				e.printStackTrace();
			}

			// Try to determine file's content type
			String contentType = null;

			try {
				contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
			} catch (IOException ex) {
				// logger.info("Could not determine file type.");
			}

			// Fallback to the default content type if type could not be determined
			if (contentType == null) {
				contentType = "application/octet-stream";
			}
			return ResponseEntity.ok()
					.contentType(MediaType.parseMediaType(contentType))
					.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
					.body(resource);
		} else {
			return ResponseEntity.notFound().build();
		}

	}
	
	@GetMapping("/pizarra/{codigoLectura}")
	public ResponseEntity<?> mostrarDocumentoLectura(@PathVariable String codigoLectura) {
		
		DocumentoPizarra documento = null;
		Map<String, Object> response = new HashMap<>();
		
		try {
			documento = iDocumentoService.obtenerDocumentoByCodigo(codigoLectura);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta en la base de datos");
			response.put("error",e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if(documento == null) {
			response.put("mensaje", "El documento de lectura con código : " .concat(codigoLectura).concat(" no existe"));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<DocumentoPizarra>(documento, HttpStatus.OK);
	}
	
	@PutMapping("/pizarra/{codigoDocumento}")
	public ResponseEntity<?> actualizarDocumentoPizarra(@RequestBody DocumentoPizarra documento,
			BindingResult result,
			@PathVariable String codigoDocumento
			) throws IOException{
		
		DocumentoPizarra documentoActual = iDocumentoService.obtenerDocumentoByCodigo(codigoDocumento);
		DocumentoPizarra documentoUpdate = null;
		Map<String, Object> response = new HashMap<>();
		
		if(result.hasErrors()) {
			List<String> errors = result.getFieldErrors().stream()
					.map(err->"El campo '" + err.getField() + "' " + err.getDefaultMessage())
					.collect(Collectors.toList());
			
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		
		if(documentoActual == null) {
			response.put("mensaje", "Error: No se pudo editar el documento de lectura : " .concat(codigoDocumento).concat(" no existe en la base de datos"));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		
		try {
			documentoActual.setDescripcionDocumento(documento.getDescripcionDocumento());
			documentoActual.setTipoDocumento(documento.getTipoDocumento());
			documentoActual.setIndicadorEstado(documento.getIndicadorEstado());
			
			documentoUpdate =  iDocumentoService.actualizarDocumentoPizarra(documentoActual);
			
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al actualizar el documento para la pizarra de lecturas");
			response.put("error",e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje", "El documento de lectuta ha sido actualizado con éxito");
		response.put("documento", documentoUpdate);
		
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
		
	}
		
}
