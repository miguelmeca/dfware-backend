package org.cmacsullana.dfwareapi.controller;

import java.util.List;

import org.cmacsullana.dfwareapi.model.entity.SituacionSolicitudRequisicion;
import org.cmacsullana.dfwareapi.model.service.interfaces.ISituacionRequisicionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/dfware")
public class SituacionSolicRequisController {

	@Autowired
	public ISituacionRequisicionService iSituacionReqService;
	
	@GetMapping("/listSituacionReq")
	public List<SituacionSolicitudRequisicion> getListSituacionReq(){
		return iSituacionReqService.getListSituacionRequisicion();
	}
}
