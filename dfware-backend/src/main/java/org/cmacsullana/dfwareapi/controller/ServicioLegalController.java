package org.cmacsullana.dfwareapi.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.cmacsullana.dfwareapi.excepciones.BusinessException;
import org.cmacsullana.dfwareapi.model.entity.ServicioLegal;
import org.cmacsullana.dfwareapi.model.service.interfaces.IServicioLegalService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/dfware")
public class ServicioLegalController {

	@Autowired
	public IServicioLegalService iServicioLegalService;
	
	private static final Logger logger = LoggerFactory.getLogger(ServicioLegalController.class);
	
	@GetMapping("/listServicioLegalActivo")
	public List<ServicioLegal> getListServicioLegalActivo(){
		return iServicioLegalService.getListServiciosActivos();
	}
	
	@GetMapping("/listServicioLegal")
	public List<ServicioLegal> getListServicioLegal(){
		return iServicioLegalService.getListServicios();
	}
	
	@PostMapping("/registrar-servicio")
	public ResponseEntity<?> grabarServicio(@RequestBody ServicioLegal servicio) {
		ServicioLegal servicioNuevo = null;
		Map<String, Object> response = new HashMap<>();
		
			
		try {
			servicioNuevo = iServicioLegalService.grabarServicio(servicio);
		}catch (DataAccessException  e) {
			response.put("mensaje", "Problema de conexión a la base de datos");
			//response.put("error",e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			//response.put("error",e.getMessage());
			System.out.println("error:->"+e.getMessage());
			logger.error(String.format("Error al grabar el registro en la base de datos. Detalle: ", 
					e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage())),e);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (BusinessException  e) {
			response.put("mensaje", "Error al grabar el registro en la base de datos");
			//response.put("error",e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			response.put("error",e.getMessage());
			logger.error(String.format("Error al grabar el registro en la base de datos. Detalle: ", e.getMessage()),e);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje", "El servicio legal ha sido creado con éxito");
		response.put("servicio", servicioNuevo);
						
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
		
	}
	
	@GetMapping("/servicio/{codigoServicio}")
	public ResponseEntity<?> mostrarServicioLegal(@PathVariable String codigoServicio) {
		
		ServicioLegal servicioLegal = null;
		Map<String, Object> response = new HashMap<>();
		
		try {
			servicioLegal = iServicioLegalService.obtenerServicioByCodigo(codigoServicio);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta en la base de datos");
			response.put("error",e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if(servicioLegal == null) {
			response.put("mensaje", "El servicio legal con código : " .concat(codigoServicio).concat(" no existe"));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<ServicioLegal>(servicioLegal, HttpStatus.OK);
	}
	
	@PutMapping("/servicio/{codigoServicio}")
	public ResponseEntity<?> actualizarServicioLegal(@Validated @RequestBody ServicioLegal servicioLegal, BindingResult result,@PathVariable String codigoServicio) {
				
		ServicioLegal servicioActual = iServicioLegalService.obtenerServicioByCodigo(codigoServicio);
		ServicioLegal servicioUpdate = null;
		Map<String, Object> response = new HashMap<>();
		
		if(result.hasErrors()) {
			
			List<String> errors = result.getFieldErrors().stream()
					.map(err->"El campo '" + err.getField() + "' " + err.getDefaultMessage())
					.collect(Collectors.toList());
			
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		
		if(servicioActual == null) {
			response.put("mensaje", "Error: No se pudo editar El Servicio Legal: " .concat(codigoServicio).concat(" no existe en la base de datos"));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		
		try {
			servicioActual.setNombreServicioLegal(servicioLegal.getNombreServicioLegal());
			servicioActual.setIndicadorEstado(servicioLegal.getIndicadorEstado());
			
			servicioUpdate =  iServicioLegalService.actualizarServicio(servicioActual);
			
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al actualizar el Servicio Legal");
			response.put("error",e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje", "El Servicio Legal ha sido actualizado con éxito");
		response.put("servicio", servicioUpdate);
		
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
		
	}
}
