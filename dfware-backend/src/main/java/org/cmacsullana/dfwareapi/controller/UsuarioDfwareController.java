package org.cmacsullana.dfwareapi.controller;

import java.util.HashMap;
import java.util.Map;

import org.cmacsullana.dfwareapi.model.entity.RequerimientoLegal;
import org.cmacsullana.dfwareapi.model.entity.UsuarioDfware;
import org.cmacsullana.dfwareapi.model.service.interfaces.IUsuarioDfwareService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/dfware")
public class UsuarioDfwareController {
	
	private static final Logger logger = LoggerFactory.getLogger(UsuarioDfwareController.class);
			
	@Autowired
	IUsuarioDfwareService usuarioService;
	
	@GetMapping("/datosUsuario/{codigoUser}")
	public ResponseEntity<?> datosUsuario(@PathVariable String codigoUser) {
		
		UsuarioDfware usuario = null;
		Map<String, Object> response = new HashMap<>();
		try {
			usuario = usuarioService.buscarUsuarioByCodigo(codigoUser);
		}catch (DataAccessException e) {
			response.put("mensaje", "Ocurrió un error al consultar a la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response,HttpStatus.NOT_FOUND);
		}
		if(usuario == null) {
			response.put("mensaje", "El usuario no se encuentra registrado en la aplicación");
			return new ResponseEntity<Map<String, Object>>(response,HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<UsuarioDfware>(usuario,HttpStatus.OK);
	}
}
