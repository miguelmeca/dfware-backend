package org.cmacsullana.dfwareapi.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.cmacsullana.dfwareapi.bean.Usuario;
import org.cmacsullana.dfwareapi.dto.JwtDTO;
import org.cmacsullana.dfwareapi.dto.LoginUsuarioDTO;
import org.cmacsullana.dfwareapi.model.entity.UsuarioDfware;
import org.cmacsullana.dfwareapi.model.service.interfaces.IBusIntgracionService;
import org.cmacsullana.dfwareapi.model.service.interfaces.IUsuarioDfwareService;
//import org.cmacsullana.dfwareapi.model.service.interfaces.IRolService;
import org.cmacsullana.dfwareapi.security.JWT.JwtProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/dfware/auth")
public class AuthController {
	
	@Autowired
	PasswordEncoder passwordEncoder;
	
	@Autowired
	AuthenticationManager authenticationManager;
	
	@Autowired
	IBusIntgracionService iUsuarioService;
	
	@Autowired
	IUsuarioDfwareService usuarioService;
	
	@Autowired
	JwtProvider jwtProvider;
	
		
		
	@PostMapping("/login")
	public ResponseEntity<?> login(@RequestBody LoginUsuarioDTO loginUsuario, BindingResult bindingResult){
						
		Map<String, Object> response = new HashMap<>();
		Authentication authentication = null;
		Usuario user = null;
		
		if(bindingResult.hasErrors()) {
			List<String> errors = bindingResult.getFieldErrors().stream()
					.map(err->"El campo '" + err.getField() + "' " + err.getDefaultMessage())
					.collect(Collectors.toList());
			
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		
		try {
			
			String usuario = loginUsuario.getNombreUsuario();
			UsuarioDfware usuarioSistema = usuarioService.buscarUsuarioByCodigo(loginUsuario.getNombreUsuario().toUpperCase());
			if(usuarioSistema!=null) {
				if(usuarioSistema.getIndicadorActivo().equals("S")) {
					authentication = authenticationManager.authenticate(
							new UsernamePasswordAuthenticationToken(loginUsuario.getNombreUsuario().toUpperCase(), loginUsuario.getPassword())
							);
					
					SecurityContextHolder.getContext().setAuthentication(authentication);
					String jwt = jwtProvider.generateToken(authentication);
					user = (Usuario)authentication.getPrincipal();
					JwtDTO jwtDTO = new JwtDTO(jwt, user.getNombre(),authentication.getAuthorities(),user.getUsuario());
					
					response.put("mensaje", "La autenticación ha sido validada con éxito");
					response.put("respuesta", "OK");
					response.put("jwtDTO", jwtDTO);
					return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
				}else {
					response.put("mensaje", "El usuario no se encuentra registrado en la aplicación");
					response.put("respuesta", "FAIL");
					return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
				}
				}else {
					response.put("mensaje", "El usuario se encuentra INACTIVO");
					response.put("respuesta", "FAIL");
					return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
				}
		}catch (Exception e) {
			response.put("mensaje", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		
	}
	
	/*@GetMapping("/obtenerUsuario/{codigoUser}")
	public ResponseEntity<?> datosUsuario(@PathVariable String codigoUser) {
		
		UsuarioDfware usuario = null;
		Map<String, Object> response = new HashMap<>();
		System.out.println("codigo enviado:"+codigoUser);
		try {
			usuario = usuarioService.buscarUsuarioByCodigo(codigoUser);
		}catch (DataAccessException e) {
			response.put("mensaje", "Ocurrió un error al consultar a la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response,HttpStatus.NOT_FOUND);
		}
		
		if(usuario == null) {
			response.put("mensaje", "El usuario no se encuentra registrado en la aplicación");
			return new ResponseEntity<Map<String, Object>>(response,HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<UsuarioDfware>(usuario,HttpStatus.OK);
	}*/
	
}
