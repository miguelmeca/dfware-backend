package org.cmacsullana.dfwareapi.controller;

import java.util.List;

import org.cmacsullana.dfwareapi.dto.ArchivoAdjuntoServicioDTO;
import org.cmacsullana.dfwareapi.model.service.interfaces.IArchivosAdjuntosService;
import org.cmacsullana.dfwareapi.utilitarios.AppUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/dfware")
public class ArchivosAdjuntoServicioController {

	private static final Logger logger = LoggerFactory.getLogger(ArchivosAdjuntoServicioController.class);
	
	@Autowired
	IArchivosAdjuntosService archivoAdjuntoService;
	
	@GetMapping("/archivosAdjuntos/{codigoServicio}")
	public List<ArchivoAdjuntoServicioDTO> getListArchivosByCodigoServicio(@PathVariable String codigoServicio){
			return archivoAdjuntoService.getListArchivosByCodigoServicio(codigoServicio);
	}
}
