package org.cmacsullana.dfwareapi.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cmacsullana.dfwareapi.bean.Adjunto;
import org.cmacsullana.dfwareapi.dto.BandejaSolcitudesTO;
import org.cmacsullana.dfwareapi.dto.HistorialSolicitudDTO;
import org.cmacsullana.dfwareapi.excepciones.BusinessException;
import org.cmacsullana.dfwareapi.excepciones.RequiredException;
import org.cmacsullana.dfwareapi.model.entity.RequerimientoLegal;
import org.cmacsullana.dfwareapi.model.service.interfaces.IHistorialSolicitudRequerimService;
import org.cmacsullana.dfwareapi.model.service.interfaces.IMailService;
import org.cmacsullana.dfwareapi.model.service.interfaces.IRequerimientoLegalService;
import org.cmacsullana.dfwareapi.model.service.interfaces.IServicioLegalService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/dfware")
public class RequerimientoLegalController {

	private static final Logger logger = LoggerFactory.getLogger(RequerimientoLegalController.class);
	
	@Autowired
	IRequerimientoLegalService iRequerimientoLegalService;
	@Autowired
	IMailService iMailService;
	@Autowired
	IServicioLegalService iServicioLegal;
	@Autowired
	IHistorialSolicitudRequerimService historialService;
	
	
	@PostMapping(value="/agrega-requerimiento-legal", consumes = {"multipart/form-data"})
	@Transactional(rollbackFor = {BusinessException.class, RequiredException.class})
	public ResponseEntity<?> grabarRequerimientoLegal(
			@RequestPart RequerimientoLegal requerimiento,
			@RequestPart List<MultipartFile> listFiles) {
		
		RequerimientoLegal requerimientoNuevo = null;
		Map<String, Object> response = new HashMap<>();
		
		String directorio = iRequerimientoLegalService.directorioAlmacenamiento();
		
		try {
			requerimiento.setRutaDirArchivo(directorio);
			requerimientoNuevo = iRequerimientoLegalService.grabarRequerimientoLegal(requerimiento);
			
			String nombreServicio = iServicioLegal.obtenerServicioByCodigo(requerimientoNuevo.getCodServLegal()).getNombreServicioLegal();
			
			String mensaje = "Usted ha registrado un pedido a Plataforma de Soporte Legal generándose el Ticket Nº:"
						+requerimientoNuevo.getCodTicketServicio()+". El registro corresponde al servicio de "
						+nombreServicio + " En breves momentos recibira una respuesta a su solicitud de atención. "
						+" De ser necesario un operador del area de Legal se comunicará con Usted.";
			String asunto = "Ticket de Solicitud de Atención a Plataforma de Soporte Legal";
			
			/*if(iMailService.enviarEmail("miguelmeca01@gmail.com", mensaje, asunto)) {
				response.put("mensaje", "Se registro la solicitud, pero ocurrión un error al enviar el correo");
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
			}*/
			
			iRequerimientoLegalService.almacenArchivos(listFiles, directorio);
			
		}catch (DataAccessException e) {
			response.put("mensaje", "Problema con la base de datos");
			logger.error(String.format("Problema con la base de datos. Detalle: ", 
					e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage())),e);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (BusinessException  e) {
			response.put("mensaje", "Error al grabar el registro en la base de datos");
			logger.error(String.format("Error al grabar el registro en la base de datos. Detalle: ", e.getMessage()),e);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "La solicitud ha sido registrada con éxito");
		response.put("ticket", requerimientoNuevo.getCodTicketServicio());
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
		
	}
	
	@GetMapping("/bandeja/{codigoUsuario}")
	@Transactional(readOnly = true)
	public List<BandejaSolcitudesTO> mostrarBandejaPorUsuario(@PathVariable String codigoUsuario) {
		
		return iRequerimientoLegalService.listaBandejaSolcitudes(codigoUsuario);
	}
	
	
	@GetMapping("/requerimientos/{usuRegTicket}")
	public ResponseEntity<?> mostrarEstadoSolicitudesHoy(@PathVariable String usuRegTicket) {
		Map<String, Object> response =iRequerimientoLegalService.estadisticaByIndEstadoSolicAndFechaRegTicketAndUsuRegTicket(usuRegTicket);
		return new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
	}
	
	@GetMapping("/visualizaRequerimiento/{numTicket}/{usuRegTicket}")
	public void visualizaSolicitud(@PathVariable String numTicket,@PathVariable String usuRegTicket) {
		
		iRequerimientoLegalService.consultaDetalleSolicitud(numTicket, usuRegTicket);
		
	}
	
	@GetMapping("/detalleRequerimiento/{numTicket}")
	public ResponseEntity<?> detalleSolicitud(@PathVariable String numTicket) {
		
		RequerimientoLegal requerimiento = null;
		Map<String, Object> response = new HashMap<>();
		
		try {
			requerimiento = iRequerimientoLegalService.detalleRequerimientoByCodTicket(numTicket);
		}catch (DataAccessException e) {
			response.put("mensaje", "Ocurrió un error al consultar a la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response,HttpStatus.NOT_FOUND);
		}
		
		if(requerimiento == null) {
			response.put("mensaje", "El Ticket Nº: "+numTicket+" no se encuentra registrado");
			return new ResponseEntity<Map<String, Object>>(response,HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<RequerimientoLegal>(requerimiento,HttpStatus.OK);
	}
	
	/*@GetMapping("/detalleRequerimiento/{numTicket}")
	public ResponseEntity<?> detalleSolicitud(@PathVariable String numTicket) {
		
		RequerimientoLegal requerimiento = null;
		Map<String, Object> response = new HashMap<>();
		
		try {
			requerimiento = iRequerimientoLegalService.detalleRequerimientoByCodTicket(numTicket);
		}catch (DataAccessException e) {
			response.put("mensaje", "Ocurrió un error al consultar a la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response,HttpStatus.NOT_FOUND);
		}
		
		if(requerimiento == null) {
			response.put("mensaje", "El Ticket Nº: "+numTicket+" no se encuentra registrado");
			return new ResponseEntity<Map<String, Object>>(response,HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<RequerimientoLegal>(requerimiento,HttpStatus.OK);
	}*/
	
	@GetMapping("/historialSolicitud/{numTicket}")
	@Transactional(readOnly = true)
	public List<HistorialSolicitudDTO> verHistorialSolicitud(@PathVariable String numTicket) {
		
		return historialService.listHistorialSolicitud(numTicket);
	}
	
}
