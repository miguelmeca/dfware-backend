package org.cmacsullana.dfwareapi.controller;

import java.util.List;

import org.cmacsullana.dfwareapi.model.entity.EstadoSolicitudRequisicion;
import org.cmacsullana.dfwareapi.model.service.interfaces.IEstadoSolicitudRequisicionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/dfware")
public class EstadoSolicRequisicionController {

	@Autowired
	public IEstadoSolicitudRequisicionService iEstadoSolicReqService;
	
	@GetMapping("/listaEstadoSolicitud")
	public List<EstadoSolicitudRequisicion> getListEstadoSolicReq(){
		
		return iEstadoSolicReqService.getListEstadoSolicitudes();
	}
}
