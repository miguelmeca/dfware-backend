package org.cmacsullana.dfwareapi.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("file:/GUPA/DFWARE 2.0/Desarrollo/BACKEND/mail.properties")
public class MailConfig {

}
