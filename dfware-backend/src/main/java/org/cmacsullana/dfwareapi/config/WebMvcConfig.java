package org.cmacsullana.dfwareapi.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@PropertySource("file:/GUPA/DFWARE 2.0/Desarrollo/BACKEND/dfware-configuracion.properties")
public class WebMvcConfig implements WebMvcConfigurer{

	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/dfware/**")
				.allowedOrigins("*")
				.allowedMethods("HEAD","GET","POST","PUT");
	}
	
}
