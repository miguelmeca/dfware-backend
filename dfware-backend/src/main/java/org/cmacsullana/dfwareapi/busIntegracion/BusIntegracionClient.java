package org.cmacsullana.dfwareapi.busIntegracion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.codec.binary.Base64;
import org.cmacsullana.dfwareapi.utilitarios.AppUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class BusIntegracionClient {

	@Value("${bus.endpointUrl}")
	private String endpointUrl;
	
	@Value("${bus.idOperacion}")
	private String idOperacion;
	
	@Value("${bus.accessUser}")
	private String accessUser;
	
	@Value("${bus.accessKey}")
	private String accessKey;
	
	@Value("${bus.accessChanel}")
	private String accessChanel;
	
	@Value("${bus.accesType}")
	private String accesType;
	
	@Value("${bus.readTimeOut}")
	private String readTimeOut;
	
	@Value("${bus.connectTimeOut}")
	private String connectTimeOut;
	
	private RestTemplate restTemplate;
	
	@PostConstruct
	private void init() {
		initRestTemplate();
	}
	
	private void initRestTemplate() { 
		restTemplate = new RestTemplate();
		MappingJackson2HttpMessageConverter jsonConverter = new MappingJackson2HttpMessageConverter();
		List<MediaType> soporteMediaTypes = new ArrayList<MediaType>();
		soporteMediaTypes.add(new MediaType("application", "json"));
		jsonConverter.setSupportedMediaTypes(soporteMediaTypes);
		jsonConverter.setObjectMapper(new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false));
		List<HttpMessageConverter<?>> listHttpMessageConverters = restTemplate.getMessageConverters();
		
		SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        requestFactory.setReadTimeout(Integer.parseInt(this.readTimeOut));
        requestFactory.setConnectTimeout(Integer.parseInt(this.connectTimeOut));
        restTemplate.setRequestFactory(requestFactory);

        restTemplate.setMessageConverters(listHttpMessageConverters);
		
	}
	
	public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    public HttpHeaders getAuthenticator() {
        String authorisation = this.accessUser+":"+this.accessKey; 
        byte[] encodedAuthorisation = Base64.encodeBase64(authorisation.getBytes());
        String authenticator = new String(encodedAuthorisation);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Authorization", "Basic " + authenticator);
        return headers;
    }

    public Map<String, Object> getResult(Map<String, Object> vars) {
        HttpEntity<Map<String, Object>> requestHttp = new HttpEntity<Map<String, Object>>(vars, getAuthenticator());
        @SuppressWarnings("unchecked")
		Map<String, Object> result = (Map<String, Object>) getRestTemplate()
									.postForObject(this.endpointUrl, requestHttp, Map.class);
        System.out.println("REQUEST: "+ AppUtil.convertMaptoJson(vars));
        System.out.println("RESPONSE: "+ AppUtil.convertMaptoJson(result));        
        return result;
    } 
	
}
