package org.cmacsullana.dfwareapi.enums;

public enum ServiceLayerOperation {

	HELPDESK_VALIDAR_USUARIO_CORE("742");
	
	private String id;
	
	private ServiceLayerOperation(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
}
