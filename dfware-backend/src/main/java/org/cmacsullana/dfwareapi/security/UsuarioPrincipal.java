package org.cmacsullana.dfwareapi.security;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

//import org.cmacsullana.dfwareapi.model.entity.Usuario;
import org.cmacsullana.dfwareapi.bean.Usuario;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class UsuarioPrincipal implements UserDetails{
	
	private static final long serialVersionUID = 1L;
	
	private String nombre;
	private String nombreUsuario;
	private String password;
	private Collection<? extends GrantedAuthority> authorities;
		
	public UsuarioPrincipal(String nombre, String nombreUsuario, String password,
			Collection<? extends GrantedAuthority> authorities) {
		this.nombre = nombre;
		this.nombreUsuario = nombreUsuario;
		this.password = password;
		this.authorities = authorities;
	}
	
	public static UsuarioPrincipal build(Usuario usuario) {
		System.out.println("usuario Principal:"+usuario.getNombre()+"-"+usuario.getUsuario()+"-"+usuario.getPassword());
		List<GrantedAuthority> authorities = usuario
				.getMyRoles()
				.stream()
				.map(rol->new SimpleGrantedAuthority(rol.getDescription())).collect(Collectors.toList());
		return new UsuarioPrincipal(usuario.getNombre(), usuario.getUsuario(), usuario.getPassword(), authorities);
		
		/*List<GrantedAuthority> authorities = usuario.
				.getRoles()
				.stream()
				.map(rol->new SimpleGrantedAuthority(rol.getRolNombre())).collect(Collectors.toList());
		return new UsuarioPrincipal(usuario.getId(), usuario.getNombre(), usuario.getNombreUsuario(), usuario.getPassword(), authorities);*/
	}
	
	public String getNombre() {
		return nombre;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		
		return authorities;
	}

	@Override
	public String getPassword() {
		
		return password;
	}

	@Override
	public String getUsername() {
		
		return nombreUsuario;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

}
