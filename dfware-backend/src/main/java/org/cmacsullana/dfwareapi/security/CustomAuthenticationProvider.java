package org.cmacsullana.dfwareapi.security;

import java.util.ArrayList;
import java.util.List;

import org.cmacsullana.dfwareapi.bean.Role;
import org.cmacsullana.dfwareapi.bean.Usuario;
import org.cmacsullana.dfwareapi.excepciones.AuthException;
import org.cmacsullana.dfwareapi.model.service.interfaces.IBusIntgracionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider{

	private static final Logger logger = LoggerFactory.getLogger(CustomAuthenticationProvider.class);
	
	@Autowired
	Usuario usuario;
	
	@Autowired
	IBusIntgracionService iBusIntgracionService;
	
	@Value("${bus.accessKey}")
	private String accessKey;
	
	@Value("${bus.accessChanel}")
	private String accessChanel;
	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		
		try {
			String usuarioDesencriptado = authentication.getName();
			String claveDesencriptada = authentication.getCredentials().toString();
			
			usuario = iBusIntgracionService.loginUsuario(usuarioDesencriptado, claveDesencriptada);
			usuario.setUsuario(usuarioDesencriptado);
			if(usuario.getCodError().equals("00")) {
				List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
				for(Role role: usuario.getMyRoles()) {
					authorities.add(new SimpleGrantedAuthority(role.getDescription()));
				}
				return new UsernamePasswordAuthenticationToken(usuario, claveDesencriptada,authorities);
			}else {
				logger.error("Error en la validación de usuario: " + usuario.getMsjError());
				throw new AuthException(usuario.getMsjError());
			}
		}catch (Exception e) {
			logger.error("Excepción en authenticate " + e.getMessage());
			throw new AuthException(e.getMessage());
		}
		
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}
