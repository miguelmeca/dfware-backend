package org.cmacsullana.dfwareapi.bean;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@SuppressWarnings("serial")
@Repository
public class Role implements Serializable{

	
	private String description;
	
	@Autowired
	private List<Usuario> myUsers;
	
	public Role() {
		
	}

	public Role(String description) {
		super();
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Usuario> getMyUsers() {
		return myUsers;
	}

	public void setMyUsers(List<Usuario> myUsers) {
		this.myUsers = myUsers;
	}
	
}
