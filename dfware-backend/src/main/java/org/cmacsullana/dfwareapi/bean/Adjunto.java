package org.cmacsullana.dfwareapi.bean;

import java.io.Serializable;

import org.springframework.web.multipart.MultipartFile;

public class Adjunto implements Serializable{

	private String carpeta;
	private MultipartFile archivo;
	
	public String getCarpeta() {
		return carpeta;
	}
	public void setCarpeta(String carpeta) {
		this.carpeta = carpeta;
	}
	public MultipartFile getArchivo() {
		return archivo;
	}
	public void setArchivo(MultipartFile archivo) {
		this.archivo = archivo;
	}
	
}
