package org.cmacsullana.dfwareapi.excepciones;

public class RequiredException extends Exception{

	private static final long serialVersionUID = 1L;
	
	public RequiredException (String mensaje) {
		super(mensaje);
	}

	public RequiredException (String mensaje, Throwable causa) {
		super(mensaje, causa);
	}
	
}
