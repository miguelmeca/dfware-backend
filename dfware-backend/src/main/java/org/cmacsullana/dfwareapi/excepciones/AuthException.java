package org.cmacsullana.dfwareapi.excepciones;

import org.springframework.security.core.AuthenticationException;

public class AuthException extends AuthenticationException{

	private static final long serialVersionUID = 1L;

	public AuthException(String mensaje) {
		super(mensaje);
	}
}
