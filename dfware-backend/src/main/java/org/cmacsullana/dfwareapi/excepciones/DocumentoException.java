package org.cmacsullana.dfwareapi.excepciones;

public class DocumentoException extends RuntimeException{

	public DocumentoException (String mensaje) {
		super(mensaje);
	}

	public DocumentoException (String mensaje, Throwable causa) {
		super(mensaje, causa);
	}
}
