package org.cmacsullana.dfwareapi.utilitarios;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.management.RuntimeErrorException;

import org.cmacsullana.dfwareapi.bean.Usuario;
import org.cmacsullana.dfwareapi.enums.ServiceLayerOperation;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.security.core.context.SecurityContextHolder;

public class AppUtil {
		
	private static String accessChanel;
	
	public static String getAccessChanel() {
		return accessChanel;
	}
	
	public static String generarCodigo(String caracterInicial, Long numRegistrosActuales, int longitudCodigo) {
		String cadena = generaCeros(longitudCodigo) + (numRegistrosActuales+1);
		String correlativo = cadena.substring(cadena.length() - longitudCodigo, cadena.length());
		String codigo = caracterInicial + correlativo;
		return codigo;
	}
	
	private static String generaCeros(int cantidad) {
		StringBuilder cadena = new StringBuilder(); 
		for(int i=0;i<cantidad;i++) {
			cadena.append("0");
		}
		return cadena.toString();
	}
	
	public static boolean validateIsNotNullAndNotEmpty(Object o) {
		return (validateIsNotNull(o) && validateIsNotEmpty(o));
	}
	
	public static boolean validateIsNotNull(Object o) {
		return o == null;
	}
	
	public static boolean validateIsNotEmpty(Object o) {
		return !validateIsEmpty(o);
	}
	
	public static boolean validateIsEmpty(Object o) {
		String d = o.toString();
		return d.trim().length() == 0;
	}
	
	public static String convertMaptoJson(Object mapJson) {
		try {
			String json = new ObjectMapper().writeValueAsString(mapJson);

			return json;
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ConstantesApp.JSON_ERROR;
	}
	
	/**
	 * 
	 * @param params
	 * @param serviceLayerOperation
	 */
	public static void addDefaultParams(Map<String, Object> params, ServiceLayerOperation serviceLayerOperation) {
		params.put(ConstantesApp.CANAL, getAccessChanel());
		try {
			Usuario user = (Usuario) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			params.put(ConstantesApp.USUARIO, user.getUsuario());
		} catch (Exception ex) {
			params.put(ConstantesApp.USUARIO, getAccessChanel());
		}
		params.put(ConstantesApp.VAR_ID_OPERACION, serviceLayerOperation.getId());
	}
	
	public static String fechaFormateada(Date fecha, String formato) {
		
		SimpleDateFormat sdf = new SimpleDateFormat(formato);
		return sdf.format(fecha);
	}
	
	public static Date fechaActual() {
		return new Date();
	}
	
	public static String generaCadenaMD5(String texto) {
		try{
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] messageDigest = md.digest(texto.getBytes());
			BigInteger no = new BigInteger(1,messageDigest);
			String hashText = no.toString(16);
			while (hashText.length()<32) {
				hashText ="0"+hashText;
				
			}
			return hashText;
		}catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
		
	}
	
}
