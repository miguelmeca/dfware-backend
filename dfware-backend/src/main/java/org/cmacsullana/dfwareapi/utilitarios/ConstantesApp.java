package org.cmacsullana.dfwareapi.utilitarios;

public class ConstantesApp {

	public static final String USUARIO = "usuario";
	public static final String VAR_ID_OPERACION = "ID_OPER";
	public static final String VI_USUARIO = "usuario";
    public static final String VI_CANAL = "canal";
	public static final String VI_PASSWORD = "password";
	public static final String CANAL = "canal";
	public static final String VI_TIPO = "tipo";
	public static final String VO_CODIGO_AGENCIA = "codigoAgencia";
	public static final String VO_CURSOR = "cursor";
	public static final String VO_STATUS = "status";
	public static final String VO_NOMBRE_USUARIO = "nombreCliente";
	public static final String VI_CODIGO_PERFIL = "codigoPerfil";
	public static final String VI_CARGO = "cargo";
	public static final String VO_MESSAGE = "message";
	public static final String VI_PERFIL = "perfil";
	public static final String VI_CODIGO_CARGO = "codigoCargo";
	public static final String VO_AGENCIA = "nombreAgencia";
	public static final String SEPARATOR = "/";
	public static final String RUTA_PRINCIPAL_ARCHIVOS = "SDC_ARCHS";
	
	
	
	
	/* Base 64 */
	public static final String JSON_ERROR = "{\"message\":\"Ha ocurrido un error con los datos [Json]\",\"status\":\"1\"}";
	
}
