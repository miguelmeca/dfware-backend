package org.cmacsullana.dfwareapi.upload;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "file")
public class UploadFiles {

	@Value("${file.uploadDir}")
	private String uploadDir;
	
	@Value("${file.rutaPadreArchivos}")
	private String dirPadreAlmacena;

	public String getUploadDir() {
		return uploadDir;
	}

	public void setUploadDir(String uploadDir) {
		this.uploadDir = uploadDir;
	}

	public String getDirPadreAlmacena() {
		return dirPadreAlmacena;
	}

	public void setDirPadreAlmacena(String dirPadreAlmacena) {
		this.dirPadreAlmacena = dirPadreAlmacena;
	}
	
}
